 view: title_tiles {
   # Or, you could make this view a derived table, like this:
   derived_table: {
     sql: SELECT 'Install' AS Install,
    'Cost' AS Cost,
    'CPI' AS CPI,
    'DAU' AS DAU,
    'ARPDAU' AS ARPDAU,
    'Revenue' AS Revenue,
    'D30 Est' AS D30,
    'D90 Est' AS D90,
    'D120 Est' AS D120,
    'D180 Est' AS D180,
    'iOS' AS iOS,
    'Android' AS Android,
    'Total' AS Total,
       ;;
   }


  dimension: total_tile {
    type: string
    sql: ${TABLE}.Total ;;
    html: <p style="color: black; font-size: 18px">{{total_tile}} </p> ;;
    #html: <div style="height: 60px; border: 3px solid #63666A; border-radius: 15px; font-family: Google Sans,Noto Sans,Noto Sans JP,Noto Sans KR,Noto Naskh Arabic,Noto Sans Thai,Noto Sans Hebrew,Noto Sans Bengali,sans-serif; padding: 8px; display: flex;">
  #<h2 style="width:100%;height:90%;text-align:center;color: #1A73E8; line-height: 1.2rem; font-size: 15px">{{total_tile2}}</h2>
  #<div>;;
   }

  dimension: android_tile {
    type: string
    sql: ${TABLE}.Android ;;
    html: <p style="color: black; font-size: 18px">{{android_tile}} </p> ;;
    #html: <div style="height: 60px; border: 3px solid #63666A; border-radius: 15px; font-family: Google Sans,Noto Sans,Noto Sans JP,Noto Sans KR,Noto Naskh Arabic,Noto Sans Thai,Noto Sans Hebrew,Noto Sans Bengali,sans-serif; padding: 8px; display: flex;">
    #<h2 style="width:100%;height:90%;text-align:center;color: #1A73E8; line-height: 1.2rem; font-size: 15px">{{total_tile2}}</h2>
    #<div>;;
  }

  dimension: ios_tile {
    type: string
    sql: ${TABLE}.iOS ;;
    html: <p style="color: black; font-size: 18px">{{ios_tile}} </p> ;;
    #html: <div style="height: 60px; border: 3px solid #63666A; border-radius: 15px; font-family: Google Sans,Noto Sans,Noto Sans JP,Noto Sans KR,Noto Naskh Arabic,Noto Sans Thai,Noto Sans Hebrew,Noto Sans Bengali,sans-serif; padding: 8px; display: flex;">
    #<h2 style="width:100%;height:90%;text-align:center;color: #1A73E8; line-height: 1.2rem; font-size: 15px">{{total_tile2}}</h2>
    #<div>;;
  }

  dimension: Install_tile {
    type: string
    sql: ${TABLE}.Install ;;
    html: <p style="color: black; font-size: 18px">{{Install_tile}} </p> ;;
    #html: <div style="height: 60px; border: 3px solid #63666A; border-radius: 15px; font-family: Google Sans,Noto Sans,Noto Sans JP,Noto Sans KR,Noto Naskh Arabic,Noto Sans Thai,Noto Sans Hebrew,Noto Sans Bengali,sans-serif; padding: 8px; display: flex;">
    #<h2 style="width:100%;height:90%;text-align:center;color: #1A73E8; line-height: 1.2rem; font-size: 15px">{{total_tile2}}</h2>
    #<div>;;
  }

  dimension: Cost_tile {
    type: string
    sql: ${TABLE}.Cost ;;
    html: <p style="color: black; font-size: 18px">{{Cost_tile}} </p> ;;
    #html: <div style="height: 60px; border: 3px solid #63666A; border-radius: 15px; font-family: Google Sans,Noto Sans,Noto Sans JP,Noto Sans KR,Noto Naskh Arabic,Noto Sans Thai,Noto Sans Hebrew,Noto Sans Bengali,sans-serif; padding: 8px; display: flex;">
    #<h2 style="width:100%;height:90%;text-align:center;color: #1A73E8; line-height: 1.2rem; font-size: 15px">{{total_tile2}}</h2>
    #<div>;;
  }


  dimension: CPI_tile {
    type: string
    sql: ${TABLE}.CPI ;;
    html: <p style="color: black; font-size: 18px">{{CPI_tile}} </p> ;;
    #html: <div style="height: 60px; border: 3px solid #63666A; border-radius: 15px; font-family: Google Sans,Noto Sans,Noto Sans JP,Noto Sans KR,Noto Naskh Arabic,Noto Sans Thai,Noto Sans Hebrew,Noto Sans Bengali,sans-serif; padding: 8px; display: flex;">
    #<h2 style="width:100%;height:90%;text-align:center;color: #1A73E8; line-height: 1.2rem; font-size: 15px">{{total_tile2}}</h2>
    #<div>;;
  }

  dimension: DAU_tile {
    type: string
    sql: ${TABLE}.DAU ;;
    html: <p style="color: black; font-size: 18px">{{DAU_tile}} </p> ;;
    #html: <div style="height: 60px; border: 3px solid #63666A; border-radius: 15px; font-family: Google Sans,Noto Sans,Noto Sans JP,Noto Sans KR,Noto Naskh Arabic,Noto Sans Thai,Noto Sans Hebrew,Noto Sans Bengali,sans-serif; padding: 8px; display: flex;">
    #<h2 style="width:100%;height:90%;text-align:center;color: #1A73E8; line-height: 1.2rem; font-size: 15px">{{total_tile2}}</h2>
    #<div>;;
  }

  dimension: ARPDAU_tile {
    type: string
    sql: ${TABLE}.ARPDAU ;;
    html: <p style="color: black; font-size: 18px">{{ARPDAU_tile}} </p> ;;
    #html: <div style="height: 60px; border: 3px solid #63666A; border-radius: 15px; font-family: Google Sans,Noto Sans,Noto Sans JP,Noto Sans KR,Noto Naskh Arabic,Noto Sans Thai,Noto Sans Hebrew,Noto Sans Bengali,sans-serif; padding: 8px; display: flex;">
    #<h2 style="width:100%;height:90%;text-align:center;color: #1A73E8; line-height: 1.2rem; font-size: 15px">{{total_tile2}}</h2>
    #<div>;;
  }


  dimension: Revenue_tile {
    type: string
    sql: ${TABLE}.Revenue ;;
    html: <p style="color: black; font-size: 18px">{{Revenue_tile}} </p> ;;
    #html: <div style="height: 60px; border: 3px solid #63666A; border-radius: 15px; font-family: Google Sans,Noto Sans,Noto Sans JP,Noto Sans KR,Noto Naskh Arabic,Noto Sans Thai,Noto Sans Hebrew,Noto Sans Bengali,sans-serif; padding: 8px; display: flex;">
    #<h2 style="width:100%;height:90%;text-align:center;color: #1A73E8; line-height: 1.2rem; font-size: 15px">{{total_tile2}}</h2>
    #<div>;;
  }

  dimension: D180_tile {
    type: string
    sql: ${TABLE}.D180 ;;
    html: <p style="color: black; font-size: 18px">{{D180_tile}} </p> ;;
    #html: <div style="height: 60px; border: 3px solid #63666A; border-radius: 15px; font-family: Google Sans,Noto Sans,Noto Sans JP,Noto Sans KR,Noto Naskh Arabic,Noto Sans Thai,Noto Sans Hebrew,Noto Sans Bengali,sans-serif; padding: 8px; display: flex;">
    #<h2 style="width:100%;height:90%;text-align:center;color: #1A73E8; line-height: 1.2rem; font-size: 15px">{{total_tile2}}</h2>
    #<div>;;
  }

  dimension: D120_tile {
    type: string
    sql: ${TABLE}.D120 ;;
    html: <p style="color: black; font-size: 18px">{{D120_tile}} </p> ;;
    #html: <div style="height: 60px; border: 3px solid #63666A; border-radius: 15px; font-family: Google Sans,Noto Sans,Noto Sans JP,Noto Sans KR,Noto Naskh Arabic,Noto Sans Thai,Noto Sans Hebrew,Noto Sans Bengali,sans-serif; padding: 8px; display: flex;">
    #<h2 style="width:100%;height:90%;text-align:center;color: #1A73E8; line-height: 1.2rem; font-size: 15px">{{total_tile2}}</h2>
    #<div>;;
  }


  dimension: D90_tile {
    type: string
    sql: ${TABLE}.D90 ;;
    html: <p style="color: black; font-size: 18px">{{D90_tile}} </p> ;;
    #html: <div style="height: 60px; border: 3px solid #63666A; border-radius: 15px; font-family: Google Sans,Noto Sans,Noto Sans JP,Noto Sans KR,Noto Naskh Arabic,Noto Sans Thai,Noto Sans Hebrew,Noto Sans Bengali,sans-serif; padding: 8px; display: flex;">
    #<h2 style="width:100%;height:90%;text-align:center;color: #1A73E8; line-height: 1.2rem; font-size: 15px">{{total_tile2}}</h2>
    #<div>;;
  }


  dimension: D30_tile {
    type: string
    sql: ${TABLE}.D30 ;;
    html: <p style="color: black; font-size: 18px">{{D30_tile}} </p> ;;
    #html: <div style="height: 60px; border: 3px solid #63666A; border-radius: 15px; font-family: Google Sans,Noto Sans,Noto Sans JP,Noto Sans KR,Noto Naskh Arabic,Noto Sans Thai,Noto Sans Hebrew,Noto Sans Bengali,sans-serif; padding: 8px; display: flex;">
    #<h2 style="width:100%;height:90%;text-align:center;color: #1A73E8; line-height: 1.2rem; font-size: 15px">{{total_tile2}}</h2>
    #<div>;;
  }



 }
