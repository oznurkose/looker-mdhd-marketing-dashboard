 view: acc_check_ltv {
   # Or, you could make this view a derived table, like this:
   derived_table: {
     sql:  WITH forecast_values as (
SELECT date(install_time) install_time, platform, installs, cost,d1_d3_exp_ltv,d1_d7_exp_ltv,d1_d30_exp_ltv,d1_d60_exp_ltv, d1_d90_exp_ltv, d1_d120_exp_ltv, d1_d180_exp_ltv FROM `mergedom-home-design.api_connection.platform_ltv_forecast_past_values` where diff = 3),
actual_values as (
select cast(date as date) install_time, platform,-- sum(installs) installs, sum(cost) cost,
sum(actual_revenue_day_1) actual_rev_day_1,sum(actual_revenue_day_3) as actual_rev_day_3,sum(actual_revenue_day_7) actual_rev_day_7,sum(actual_revenue_day_30) actual_rev_day_30, sum(actual_revenue_day_60) actual_rev_day_60, sum(actual_revenue_day_90) actual_rev_day_90,sum(actual_revenue_day_120) actual_rev_day_120, sum(actual_revenue_day_180) actual_rev_day_180
FROM `mergedom-home-design.api_connection.appsflyer_cohort_revenue`
group by 1, 2),
forecast_values_2 as
(SELECT date(install_time) install_time, platform, installs, cost,d1_d3_exp_ltv,d1_d7_exp_ltv, d1_d30_exp_ltv,d1_d60_exp_ltv, d1_d90_exp_ltv, d1_d120_exp_ltv, d1_d180_exp_ltv FROM `mergedom-home-design.api_connection.platform_ltv_forecast` where date_diff(date(run_time),date(install_time),day) = 3)

select install_time, platform,1 as day_diff, installs, cost,d1_d3_exp_ltv,actual_rev_day_3,d1_d7_exp_ltv,actual_rev_day_7, d1_d30_exp_ltv, actual_rev_day_30,d1_d60_exp_ltv, actual_rev_day_60, d1_d90_exp_ltv, actual_rev_day_90, d1_d120_exp_ltv, actual_rev_day_120, d1_d180_exp_ltv, actual_rev_day_180
from actual_values
join (select * from forecast_values where install_time < '2022-02-18' UNION ALL select * from forecast_values_2) using (install_time, platform)
       ;;
   }

   # Define your dimensions and measures here, like this:
   dimension: install_time {
     type: date
     sql: cast(date(${TABLE}.install_time) as timestamp) ;;
   }


   dimension: platform {
     type: string
     sql: ${TABLE}.platform ;;
   }

  dimension: day_diff {
    type: number
    sql: ${TABLE}.day_diff ;;
  }

   measure: installs {
     type: number
     sql: sum(${TABLE}.installs) ;;
   }

  measure: cost {
    type: number
    sql: sum(${TABLE}.cost) ;;
  }

  measure: d1_d30_diff{
    type: number
    sql: ${predicted_roas_day_30}-${actual_roas_day_30} ;;
  }

  measure: d1_d30_diff_abs{
    type: number
    sql: abs(${d1_d30_diff}) ;;
    html:
    {% if acc_check_ltv.d1_d30_diff._value >= 0 %}

    <div style="background-color: rgba(0,210,15,0.5); text-align:center"><a  style="color: black" target="_new">{{rendered_value}} </a></div>
    {% else %}
    <div style="background-color: rgba(255,0,0,0.5); text-align:center"><a  style="color: black" target="_new">{{rendered_value }} </a></div>

    {% endif %}

    ;;
  }

  measure: d1_d60_diff{
    type: number
    sql: ${predicted_roas_day_60}-${actual_roas_day_60} ;;
  }

  measure: d1_d90_diff{
    type: number
    sql: ${predicted_roas_day_90}-${actual_roas_day_90} ;;
  }

  measure: d1_d120_diff{
    type: number
    sql: ${predicted_roas_day_120}-${actual_roas_day_120} ;;
  }

  measure: d1_d180_diff{
    type: number
    sql: ${predicted_roas_day_180}-${actual_roas_day_180} ;;
  }



  measure: d1_d3_exp_ltv {
    type: number
    sql: sum(${TABLE}.d1_d3_exp_ltv) ;;
  }



  measure: actual_rev_day_3 {
    type: number
    sql: sum(${TABLE}.actual_rev_day_3) ;;
  }
  measure: actual_roas_day_3 {
    type: number
    sql: sum(${TABLE}.actual_rev_day_3)/sum(${TABLE}.cost) ;;
  }
  measure: predicted_roas_day_3 {
    type: number
    sql: sum(${TABLE}.d1_d3_exp_ltv)/sum(${TABLE}.cost) ;;
  }



  measure: d1_d7_exp_ltv {
    type: number
    sql: sum(${TABLE}.d1_d7_exp_ltv) ;;
  }



  measure: actual_rev_day_7 {
    type: number
    sql: sum(${TABLE}.actual_rev_day_7) ;;
  }
  measure: actual_roas_day_7 {
    type: number
    sql: sum(${TABLE}.actual_rev_day_7)/sum(${TABLE}.cost) ;;
  }
  measure: predicted_roas_day_7 {
    type: number
    sql: sum(${TABLE}.d1_d7_exp_ltv)/sum(${TABLE}.cost) ;;
  }


  measure: d1_d30_exp_ltv {
    type: number
    sql: sum(${TABLE}.d1_d30_exp_ltv) ;;
  }



  measure: actual_rev_day_30 {
    type: number
    sql: sum(${TABLE}.actual_rev_day_30) ;;
  }
  measure: actual_roas_day_30 {
    type: number
    sql: sum(${TABLE}.actual_rev_day_30)/sum(${TABLE}.cost) ;;
  }
  measure: predicted_roas_day_30 {
    type: number
    sql: sum(${TABLE}.d1_d30_exp_ltv)/sum(${TABLE}.cost) ;;
  }

  measure: d1_d60_exp_ltv {
    type: number
    sql: sum(${TABLE}.d1_d60_exp_ltv) ;;
  }
  measure: actual_rev_day_60 {
    type: number
    sql: sum(${TABLE}.actual_rev_day_60) ;;
  }
  measure: actual_roas_day_60 {
    type: number
    sql: sum(${TABLE}.actual_rev_day_60)/sum(${TABLE}.cost) ;;
  }
  measure: predicted_roas_day_60 {
    type: number
    sql: sum(${TABLE}.d1_d60_exp_ltv)/sum(${TABLE}.cost) ;;
  }
  measure: d1_d90_exp_ltv {
    type: number
    sql: sum(${TABLE}.d1_d90_exp_ltv) ;;
  }
  measure: actual_rev_day_90 {
    type: number
    sql: sum(${TABLE}.actual_rev_day_90) ;;
  }
  measure: actual_roas_day_90 {
    type: number
    sql: sum(${TABLE}.actual_rev_day_90)/sum(${TABLE}.cost) ;;
  }
  measure: predicted_roas_day_90 {
    type: number
    sql: sum(${TABLE}.d1_d90_exp_ltv)/sum(${TABLE}.cost) ;;
  }
  measure: d1_d120_exp_ltv {
    type: number
    sql: sum(${TABLE}.d1_d120_exp_ltv) ;;
  }
  measure: actual_rev_day_120 {
    type: number
    sql: sum(${TABLE}.actual_rev_day_120) ;;
  }
  measure: actual_roas_day_120 {
    type: number
    sql: sum(${TABLE}.actual_rev_day_120)/sum(${TABLE}.cost) ;;
  }
  measure: predicted_roas_day_120 {
    type: number
    sql: sum(${TABLE}.d1_d120_exp_ltv)/sum(${TABLE}.cost) ;;
  }
  measure: d1_d180_exp_ltv {
    type: number
    sql: sum(${TABLE}.d1_d180_exp_ltv) ;;
  }
  measure: actual_rev_day_180 {
    type: number
    sql: sum(${TABLE}.actual_rev_day_180) ;;
  }
  measure: actual_roas_day_180 {
    type: number
    sql: sum(${TABLE}.actual_rev_day_180)/sum(${TABLE}.cost) ;;
  }
  measure: predicted_roas_day_180 {
    type: number
    sql: sum(${TABLE}.d1_d180_exp_ltv)/sum(${TABLE}.cost) ;;
  }
 }
