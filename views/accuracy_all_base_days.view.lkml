 view: accuracy_all_base_days {
   # Or, you could make this view a derived table, like this:
   derived_table: {
     sql:
WITH exp_ltv AS (


SELECT DATE(install_time) install_date,
DATE_DIFF( DATE_ADD(DATE(run_time), INTERVAL -2 DAY), DATE(install_time), day) as day_diff,
platform,
cost,

CASE WHEN DATE_DIFF( DATE_ADD(DATE(run_time), INTERVAL -2 DAY), DATE(install_time), day)=1 THEN d1_d180_exp_ltv ELSE NULL END AS d1_d180_exp_ltv,

CASE WHEN DATE_DIFF( DATE_ADD(DATE(run_time), INTERVAL -2 DAY), DATE(install_time), day)=3 THEN d3_d180_exp_ltv ELSE NULL END AS d3_d180_exp_ltv,


CASE WHEN DATE_DIFF( DATE_ADD(DATE(run_time), INTERVAL -2 DAY), DATE(install_time), day)=7 THEN d7_d180_exp_ltv ELSE NULL END AS d7_d180_exp_ltv,

FIRST_VALUE(cost) OVER (PARTITION BY  DATE(install_time), platform ORDER BY run_time DESC) last_cost,


FROM `mergedom-home-design.api_connection.platform_ltv_forecast`
WHERE  DATE_DIFF( DATE(run_time), DATE(install_time), day) IN (3, 5, 9)
AND DATE(install_time) >= '2022-02-18' AND
 DATE(install_time) < '2022-08-01'
--ORDER BY 1

UNION ALL


SELECT DATE(install_time) install_date,
DATE_DIFF( DATE_ADD(DATE(run_time), INTERVAL -2 DAY), DATE(install_time), day) as day_diff,
platform,
cost,

CASE WHEN DATE_DIFF( DATE_ADD(DATE(run_time), INTERVAL -2 DAY), DATE(install_time), day)=1 THEN d1_d180_exp_ltv ELSE NULL END AS d1_d180_exp_ltv,

CASE WHEN DATE_DIFF( DATE_ADD(DATE(run_time), INTERVAL -2 DAY), DATE(install_time), day)=3 THEN d3_d180_exp_ltv ELSE NULL END AS d3_d180_exp_ltv,


CASE WHEN DATE_DIFF( DATE_ADD(DATE(run_time), INTERVAL -2 DAY), DATE(install_time), day)=7 THEN d7_d180_exp_ltv ELSE NULL END AS d7_d180_exp_ltv,

FIRST_VALUE(cost) OVER (PARTITION BY  DATE(install_time), platform ORDER BY run_time DESC) last_cost,


FROM `mergedom-home-design.api_connection.platform_ltv_forecast_past_values`
WHERE  DATE_DIFF( DATE(run_time), DATE(install_time), day) IN (3, 5, 9)
AND DATE(install_time) < '2022-02-18'),

actual_values as (
select cast(date as date) install_date, platform, sum(installs) installs, sum(cost) cost,
sum(actual_revenue_day_1) actual_rev_day_1,sum(actual_revenue_day_3) as actual_rev_day_3,sum(actual_revenue_day_7) actual_rev_day_7,sum(actual_revenue_day_30) actual_rev_day_30, sum(actual_revenue_day_60) actual_rev_day_60, sum(actual_revenue_day_90) actual_rev_day_90,sum(actual_revenue_day_120) actual_rev_day_120, sum(actual_revenue_day_180) actual_rev_day_180,
sum(actual_revenue_day_180)/sum(cost) actual_roas_day_180, sum(actual_revenue_day_1)/sum(cost) actual_roas_day_1,sum(actual_revenue_day_3)/sum(cost) actual_roas_day_3,sum(actual_revenue_day_7)/sum(cost) actual_roas_day_7
FROM `mergedom-home-design.api_connection.appsflyer_cohort_revenue`
GROUP BY 1, 2),

exp_roas_ AS (
SELECT exp_ltv.install_date, platform, last_cost AS cost, SUM(d1_d180_exp_ltv)/last_cost d1_d180_exp_roas,
SUM(d3_d180_exp_ltv)/last_cost d3_d180_exp_roas,
SUM(d7_d180_exp_ltv)/last_cost d7_d180_exp_roas,
FROM exp_ltv
GROUP BY 1,2,3
ORDER BY 1),

new_pred AS (
SELECT date AS install_date,
day_diff,
platform, cost,
CASE WHEN day_diff = 1 THEN pred_180 ELSE NULL END AS d1_d180_exp_roas,
CASE WHEN day_diff = 3 THEN pred_180 ELSE NULL END AS d3_d180_exp_roas,
CASE WHEN day_diff = 7 THEN pred_180 ELSE NULL END AS d7_d180_exp_roas,
FIRST_VALUE(cost) OVER (PARTITION BY date, platform ORDER BY day_diff DESC)
FROM `mergedom-home-design.api_connection.campaign_aggregate_roas_prediction`
WHERE day_diff IN (1,3,7)
AND date >= '2022-08-01'),

new_pred_roas AS (

SELECT install_date, platform, cost,
SUM(d1_d180_exp_roas) d1_d180_exp_roas,
SUM(d3_d180_exp_roas) d3_d180_exp_roas,
SUM(d7_d180_exp_roas) d7_d180_exp_roas
FROM new_pred
GROUP BY 1,2,3),

exp_roas AS (SELECT install_date, platform, cost,
d1_d180_exp_roas,
d3_d180_exp_roas,
d7_d180_exp_roas
FROM exp_roas_

UNION ALL

SELECT install_date, platform, cost,
d1_d180_exp_roas,
d3_d180_exp_roas,
d7_d180_exp_roas
FROM new_pred_roas)




SELECT exp_.*, av.actual_roas_day_180, av.actual_roas_day_1, av.actual_roas_day_3, av.actual_roas_day_7
FROM exp_roas AS exp_
LEFT JOIN actual_values av ON exp_.install_date = av.install_date AND  exp_.platform = av.platform
ORDER BY 1



       ;;
   }

   # Define your dimensions and measures here, like this:

   dimension: platform {
     type: string
     sql: ${TABLE}.platform ;;
   }

   dimension_group: install_date {
     type: time
     timeframes: [date, week, month, year]
     sql: CAST(${TABLE}.install_date AS timestamp) ;;
   }

   measure: cost {
    type:  number
     sql:  sum(${TABLE}.cost) ;;
   }

  measure: d1_d180_exp_roas {
    type: number
    value_format: "0.00%"
    sql:  SUM(${TABLE}.d1_d180_exp_roas) ;;
  }

  measure: d3_d180_exp_roas {
    type: number
    value_format: "0.00%"
    sql:  SUM(${TABLE}.d3_d180_exp_roas) ;;
  }

  measure: d7_d180_exp_roas {
    type: number
    value_format: "0.00%"
    sql:  SUM(${TABLE}.d7_d180_exp_roas) ;;
  }

  measure: actual_roas_day_180 {
    type: number
    value_format: "0.00%"
    sql:  SUM(${TABLE}.actual_roas_day_180 );;
  }

  measure: actual_roas_day_1 {
    type: number
    value_format: "0.00%"
    sql:  SUM(${TABLE}.actual_roas_day_1) ;;
  }

  measure: actual_roas_day_3 {
    type: number
    value_format: "0.00%"
    sql: SUM( ${TABLE}.actual_roas_day_3) ;;
  }

  measure: actual_roas_day_7 {
    type: number
    value_format: "0.00%"
    sql:  SUM(${TABLE}.actual_roas_day_7 );;
  }


 }
