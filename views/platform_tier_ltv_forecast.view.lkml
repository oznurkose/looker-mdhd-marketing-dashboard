#view: platform_tier_ltv_forecast {
#  sql_table_name: `api_connection.platform_tier_ltv_forecast`
#    ;;

view: platform_tier_ltv_forecast {
  # Or, you could make this view a derived table, like this:
  derived_table: {
    sql: select * EXCEPT(dau),
date_diff( date(run_time), date(install_time)+2, day) as day_diff,
case when dau = 0 THEN LEAD(dau) OVER(PARTITION BY install_time,app_name,platform,tier ORDER BY date_diff( date(run_time), date(install_time)+2, day), run_time DESC) ELSE dau END AS dau,
    concat("D",date_diff( date(run_time), date(install_time)+2, day)) as day_diff_str FROM `mergedom-home-design.api_connection.platform_tier_ltv_forecast` where date_diff( date(run_time), date(install_time)+2, day) >= 0


             ;;


  }

  dimension: ad_revenue {
    type: number
    sql: ${TABLE}.ad_revenue ;;
  }

  dimension: day_diff {
    type: number
    sql: ${TABLE}.day_diff ;;
  }

  dimension: tier_ordered {
    type: number
    sql: case when ${TABLE}.tier = "US" THEN 0
      when  ${TABLE}.tier = "Tier1" THEN 1
      when  ${TABLE}.tier = "Tier2" THEN 2
      when  ${TABLE}.tier = "Tier3" THEN 3
      when  ${TABLE}.tier = "Tier4" THEN 4
      when  ${TABLE}.tier = "Tier5" THEN 5
      when  ${TABLE}.tier = "ROW" THEN 6 END;;
  }

  dimension: day_diff_str {
    type:  string
    sql:  ${TABLE}.day_diff_str ;;
  }

  dimension: af_purchase_sum_day_0 {
    type: number
    sql: ${TABLE}.af_purchase_sum_day_0 ;;
  }

  dimension: af_purchase_sum_day_1 {
    type: number
    sql: ${TABLE}.af_purchase_sum_day_1 ;;
  }

  dimension: af_purchase_sum_day_3 {
    type: number
    sql: ${TABLE}.af_purchase_sum_day_3 ;;
  }

  dimension: af_purchase_sum_day_7 {
    type: number
    sql: ${TABLE}.af_purchase_sum_day_7 ;;
  }

  dimension: app_name {
    type: string
    sql: ${TABLE}.app_name ;;
  }

  dimension: app_opened_monetized {
    type: number
    sql: ${TABLE}.app_opened_monetized ;;
  }

  dimension: cost {
    type: number
    sql: ${TABLE}.cost ;;
  }

  dimension: d0_d120_exp_ltv {
    type: number
    sql: ${TABLE}.d0_d120_exp_ltv ;;
  }

  dimension: d0_d14_exp_ltv {
    type: number
    sql: ${TABLE}.d0_d14_exp_ltv ;;
  }

  dimension: d0_d180_exp_ltv {
    type: number
    sql: ${TABLE}.d0_d180_exp_ltv ;;
  }

  dimension: d0_d270_exp_ltv {
    type: number
    sql: ${TABLE}.d0_d270_exp_ltv ;;
  }

  dimension: d0_d30_exp_ltv {
    type: number
    sql: ${TABLE}.d0_d30_exp_ltv ;;
  }

  dimension: d0_d360_exp_ltv {
    type: number
    sql: ${TABLE}.d0_d360_exp_ltv ;;
  }

  dimension: d0_d3_exp_ltv {
    type: number
    sql: ${TABLE}.d0_d3_exp_ltv ;;
  }

  dimension: d0_d60_exp_ltv {
    type: number
    sql: ${TABLE}.d0_d60_exp_ltv ;;
  }

  dimension: d0_d720_exp_ltv {
    type: number
    sql: ${TABLE}.d0_d720_exp_ltv ;;
  }

  dimension: d0_d7_exp_ltv {
    type: number
    sql: ${TABLE}.d0_d7_exp_ltv ;;
  }

  dimension: d0_d90_exp_ltv {
    type: number
    sql: ${TABLE}.d0_d90_exp_ltv ;;
  }

  dimension: d13_iap_rev {
    type: number
    sql: ${TABLE}.d13_iap_rev ;;
  }

  dimension: d13_rev {
    type: number
    sql: ${TABLE}.d13_rev ;;
  }

  dimension: d1_d120_exp_ltv {
    type: number
    sql: ${TABLE}.d1_d120_exp_ltv ;;
  }

  dimension: d1_d14_exp_ltv {
    type: number
    sql: ${TABLE}.d1_d14_exp_ltv ;;
  }

  dimension: d1_d180_exp_ltv {
    type: number
    sql: ${TABLE}.d1_d180_exp_ltv ;;
  }

  dimension: d1_d270_exp_ltv {
    type: number
    sql: ${TABLE}.d1_d270_exp_ltv ;;
  }

  dimension: d1_d30_exp_ltv {
    type: number
    sql: ${TABLE}.d1_d30_exp_ltv ;;
  }

  dimension: d1_d360_exp_ltv {
    type: number
    sql: ${TABLE}.d1_d360_exp_ltv ;;
  }

  dimension: d1_d3_exp_ltv {
    type: number
    sql: ${TABLE}.d1_d3_exp_ltv ;;
  }

  dimension: d1_d60_exp_ltv {
    type: number
    sql: ${TABLE}.d1_d60_exp_ltv ;;
  }

  dimension: d1_d720_exp_ltv {
    type: number
    sql: ${TABLE}.d1_d720_exp_ltv ;;
  }

  dimension: d1_d7_exp_ltv {
    type: number
    sql: ${TABLE}.d1_d7_exp_ltv ;;
  }

  dimension: d1_d90_exp_ltv {
    type: number
    sql: ${TABLE}.d1_d90_exp_ltv ;;
  }

  dimension: d3_d120_exp_ltv {
    type: number
    sql: ${TABLE}.d3_d120_exp_ltv ;;
  }

  dimension: d3_d14_exp_ltv {
    type: number
    sql: ${TABLE}.d3_d14_exp_ltv ;;
  }

  dimension: d3_d180_exp_ltv {
    type: number
    sql: ${TABLE}.d3_d180_exp_ltv ;;
  }

  dimension: d3_d270_exp_ltv {
    type: number
    sql: ${TABLE}.d3_d270_exp_ltv ;;
  }

  dimension: d3_d30_exp_ltv {
    type: number
    sql: ${TABLE}.d3_d30_exp_ltv ;;
  }

  dimension: d3_d360_exp_ltv {
    type: number
    sql: ${TABLE}.d3_d360_exp_ltv ;;
  }

  dimension: d3_d60_exp_ltv {
    type: number
    sql: ${TABLE}.d3_d60_exp_ltv ;;
  }

  dimension: d3_d720_exp_ltv {
    type: number
    sql: ${TABLE}.d3_d720_exp_ltv ;;
  }

  dimension: d3_d7_exp_ltv {
    type: number
    sql: ${TABLE}.d3_d7_exp_ltv ;;
  }

  dimension: d3_d90_exp_ltv {
    type: number
    sql: ${TABLE}.d3_d90_exp_ltv ;;
  }

  dimension: d7_d120_exp_ltv {
    type: number
    sql: ${TABLE}.d7_d120_exp_ltv ;;
  }

  dimension: d7_d14_exp_ltv {
    type: number
    sql: ${TABLE}.d7_d14_exp_ltv ;;
  }

  dimension: d7_d180_exp_ltv {
    type: number
    sql: ${TABLE}.d7_d180_exp_ltv ;;
  }

  dimension: d7_d270_exp_ltv {
    type: number
    sql: ${TABLE}.d7_d270_exp_ltv ;;
  }

  dimension: d7_d30_exp_ltv {
    type: number
    sql: ${TABLE}.d7_d30_exp_ltv ;;
  }

  dimension: d7_d360_exp_ltv {
    type: number
    sql: ${TABLE}.d7_d360_exp_ltv ;;
  }

  dimension: d7_d60_exp_ltv {
    type: number
    sql: ${TABLE}.d7_d60_exp_ltv ;;
  }

  dimension: d7_d720_exp_ltv {
    type: number
    sql: ${TABLE}.d7_d720_exp_ltv ;;
  }

  dimension: d7_d90_exp_ltv {
    type: number
    sql: ${TABLE}.d7_d90_exp_ltv ;;
  }

  dimension: dau {
    type: number
    sql: ${TABLE}.dau ;;
  }

  dimension: facebook_ad_shown_monetized {
    type: number
    sql: ${TABLE}.facebook_ad_shown_monetized ;;
  }

  dimension: iap_revenue {
    type: number
    sql: ${TABLE}.iap_revenue ;;
  }

  dimension: install_time {
    type: string
    sql: ${TABLE}.install_time ;;
  }

  dimension: install_date {
    type: date_time
    sql: cast(date(${TABLE}.install_time) as timestamp);;
  }

  dimension: formattted_install_date {
    type: string
    sql: date(${TABLE}.install_time);;
    html: {{ rendered_value | date: "%b - %d, %a" }} ;;
  }

  dimension: installs {
    type: number
    sql: ${TABLE}.installs ;;
  }

  dimension: platform {
    type: string
    sql:  case when ${TABLE}.platform = "android" then "Android" Else "IOS" End ;;
  }


  dimension: ret_d1 {
    type: number
    sql: ${TABLE}.ret_d1 ;;
  }

  dimension: revenue_sum_day_0 {
    type: number
    sql: ${TABLE}.revenue_sum_day_0 ;;
  }

  dimension: revenue_sum_day_1 {
    type: number
    sql: ${TABLE}.revenue_sum_day_1 ;;
  }

  dimension: revenue_sum_day_3 {
    type: number
    sql: ${TABLE}.revenue_sum_day_3 ;;
  }

  dimension: revenue_sum_day_7 {
    type: number
    sql: ${TABLE}.revenue_sum_day_7 ;;
  }

  dimension_group: run {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.run_time ;;
  }

  dimension: tier {
    type: string
    sql: ${TABLE}.tier ;;
  }

  dimension: total_revenue {
    type: number
    sql: ${TABLE}.total_revenue ;;
  }

  measure: count {
    type: count
    drill_fields: [app_name]
  }

  measure: arpdau_total {
    type:  number
    value_format: "$0.00"
    sql:  case when SUM(${dau}) = 0 THEN 0 ELSE (SUM(${total_revenue}) - SUM( ${iap_revenue})*0.3) / SUM(${dau}) END ;;
    html:
    {% if platform_tier_ltv_forecast_lag.ARPDAU_change._value >= 0 %}

    <div style="background-color: rgba(0,210,15,{{platform_tier_ltv_forecast_lag.ARPDAU_scaled._value}}); text-align:center"><a  style="color: black" target="_new">{{rendered_value}} </a></div>
    {% else %}
    <div style="background-color: rgba(255,0,0,{{platform_tier_ltv_forecast_lag.ARPDAU_scaled._value}}); text-align:center"><a  style="color: black" target="_new">{{rendered_value }} </a></div>

    {% endif %}

    ;;
  }

  measure: max_cpitotal {
    type:  number
    sql:  max(sum(${cost}) / sum(${installs})) ;;
  }

  measure: sum_cost {
    type:  number
    sql:sum(${cost}) ;;
  }

  measure: sum_dau {
    type:  number
    sql:sum(${dau}) ;;
  }

  measure: sum_retd1 {
    type:  number
    value_format: "0.00%"
    sql:sum(${ret_d1}) / sum(${installs}) ;;
  }



  measure: min_cpitotal {
    type:  number
    sql:  min(sum(${cost}) / sum(${installs})) ;;
  }


  measure: cpi_total {
    type:  number
    value_format: "$0.00"
    sql:  sum(${cost}) / sum(${installs}) ;;
    html:
    {% if platform_tier_ltv_forecast_lag.CPI_change._value >= 0 %}

    <div style="background-color: rgba(0,210,15,{{platform_tier_ltv_forecast_lag.CPI_change_scaled._value}}); text-align:center"><a  style="color: black" target="_new">{{rendered_value}} </a></div>
    {% else %}
    <div style="background-color: rgba(255,0,0,{{platform_tier_ltv_forecast_lag.CPI_change_scaled._value}}); text-align:center"><a  style="color: black" target="_new">{{rendered_value }} </a></div>

    {% endif %}

    ;;}

  measure: cpi_total2 {
    type:  number
    value_format: "$0.00"
    sql:  sum(${cost}) / sum(${installs}) ;;}

  measure: revenue_total {
    type:  number
    value_format: "#,##0"
    sql:  sum( ${total_revenue} ) - sum(${iap_revenue})*0.3 ;;
    html:
    {% if platform_tier_ltv_forecast_lag.totalrev_change._value >= 0 %}

    <div style="background-color: rgba(0,210,15,{{platform_tier_ltv_forecast_lag.totalrev_change_scaled._value}}); text-align:center"><a  style="color: black" target="_new">{{rendered_value}} </a></div>
    {% else %}
    <div style="background-color: rgba(255,0,0,{{platform_tier_ltv_forecast_lag.totalrev_change_scaled._value}}); text-align:center"><a  style="color: black" target="_new">{{rendered_value }} </a></div>

    {% endif %}

    ;;
  }

  measure: d0_roas {
    type:  number
    value_format: "0.00%"
    sql:  case when SUM(${cost}) != 0 then (SUM( ${revenue_sum_day_0} ) - SUM( ${af_purchase_sum_day_0} )*0.3) / SUM(${cost}) else null end ;;
    html:
    {% if platform_tier_ltv_forecast_lag.d0Roas_change._value >= 0 %}

    <div style="background-color: rgba(0,210,15,{{platform_tier_ltv_forecast_lag.totalrev_change_scaled._value}}); text-align:center"><a  style="color: black" target="_new">{{rendered_value}} </a></div>

    {% else %}
    <div style="background-color: rgba(255,0,0,{{platform_tier_ltv_forecast_lag.d0Roas_change_scaled._value}}); text-align:center"><a  style="color: black" target="_new">{{rendered_value }} </a></div>

    {% endif %}

    ;;

  }



  measure: d30_exp_roas {
    type:  number
    value_format: "0.00%"
    sql:   case when MIN(${day_diff}) <= 0 and SUM(${cost}) != 0 THEN (SUM( ${d0_d30_exp_ltv} ) / SUM(${cost}))
    when MIN(${day_diff}) < 3 and SUM(${cost}) != 0 THEN (SUM( ${d1_d30_exp_ltv} ) / SUM(${cost}))
    when MIN(${day_diff}) <7 and SUM(${cost}) != 0 THEN (SUM( ${d3_d30_exp_ltv} ) / SUM(${cost}))
    when MIN(${day_diff}) >= 7 and SUM(${cost}) != 0 THEN (SUM( ${d7_d30_exp_ltv} ) / SUM(${cost}))  ELSE null END;;
    html:
    {% if d180_exp_roas._value >= 1 %}

    <div style="background-color: rgba(0,210,15,{{d180_scaled._value}}); text-align:center"><a  style="color: black" target="_new">{{rendered_value}} </a></div>
    {% elsif d180_exp_roas._value >= 0.85 %}
    <div style="background-color: rgba(250,240,53,{{d180_scaled._value}}); text-align:center"><a  style="color: black" target="_new">{{rendered_value }} </a></div>
    {% else %}
    <div style="background-color: rgba(255,0,0,{{d180_scaled._value}}); text-align:center"><a  style="color: black" target="_new">{{rendered_value }} </a></div>

    {% endif %}

    ;;
  }

  measure: d60_exp_roas {
    type:  number
    value_format: "0.00%"
    sql:    case when MIN(${day_diff}) <= 0 and SUM(${cost}) != 0 THEN (SUM( ${d0_d60_exp_ltv} ) / SUM(${cost}))
    when MIN(${day_diff}) < 3 and SUM(${cost}) != 0 THEN (SUM( ${d1_d60_exp_ltv} ) / SUM(${cost}))
    when MIN(${day_diff}) <7 and SUM(${cost}) != 0 THEN (SUM( ${d3_d60_exp_ltv} ) / SUM(${cost}))
    when MIN(${day_diff}) >= 7 and SUM(${cost}) != 0 THEN (SUM( ${d7_d60_exp_ltv} ) / SUM(${cost}))  ELSE null END;;
    html:
    {% if d180_exp_roas._value >= 1 %}

    <div style="background-color: rgba(0,210,15,{{d180_scaled._value}}); text-align:center"><a  style="color: black" target="_new">{{rendered_value}} </a></div>
    {% elsif d180_exp_roas._value >= 0.85 %}
    <div style="background-color: rgba(250,240,53,{{d180_scaled._value}}); text-align:center"><a  style="color: black" target="_new">{{rendered_value }} </a></div>
    {% else %}
    <div style="background-color: rgba(255,0,0,{{d180_scaled._value}}); text-align:center"><a  style="color: black" target="_new">{{rendered_value }} </a></div>

    {% endif %}

    ;;
  }

  measure: d90_exp_roas {
    type:  number
    value_format: "0.00%"
    sql:    case when MIN(${day_diff}) <= 0 and SUM(${cost}) != 0 THEN (SUM( ${d0_d90_exp_ltv} ) / SUM(${cost}))
    when MIN(${day_diff}) < 3 and SUM(${cost}) != 0 THEN (SUM( ${d1_d90_exp_ltv} ) / SUM(${cost}))
    when MIN(${day_diff}) <7 and SUM(${cost}) != 0 THEN (SUM( ${d3_d90_exp_ltv} ) / SUM(${cost}))
    when MIN(${day_diff}) >= 7 and SUM(${cost}) != 0 THEN (SUM( ${d7_d90_exp_ltv} ) / SUM(${cost}))  ELSE null END;;
    html:
    {% if d180_exp_roas._value >= 1 %}

    <div style="background-color: rgba(0,210,15,{{d180_scaled._value}}); text-align:center"><a  style="color: black" target="_new">{{rendered_value}} </a></div>
    {% elsif d180_exp_roas._value >= 0.85 %}
    <div style="background-color: rgba(250,240,53,{{d180_scaled._value}}); text-align:center"><a  style="color: black" target="_new">{{rendered_value }} </a></div>
    {% else %}
    <div style="background-color: rgba(255,0,0,{{d180_scaled._value}}); text-align:center"><a  style="color: black" target="_new">{{rendered_value }} </a></div>

    {% endif %}

    ;;
  }
  measure: d120_exp_roas {
    type:  number
    value_format: "0.00%"
    sql:    case when MIN(${day_diff}) <= 0 and SUM(${cost}) != 0 THEN (SUM( ${d0_d120_exp_ltv} ) / SUM(${cost}))
    when MIN(${day_diff}) < 3 and SUM(${cost}) != 0 THEN (SUM( ${d1_d120_exp_ltv} ) / SUM(${cost}))
    when MIN(${day_diff}) <7 and SUM(${cost}) != 0 THEN (SUM( ${d3_d120_exp_ltv} ) / SUM(${cost}))
    when MIN(${day_diff}) >= 7 and SUM(${cost}) != 0 THEN (SUM( ${d7_d120_exp_ltv} ) / SUM(${cost}))  ELSE null END;;
    html:
    {% if d180_exp_roas._value >= 1 %}

    <div style="background-color: rgba(0,210,15,{{d180_scaled._value}}); text-align:center"><a  style="color: black" target="_new">{{rendered_value}} </a></div>
    {% elsif d180_exp_roas._value >= 0.85 %}
    <div style="background-color: rgba(250,240,53,{{d180_scaled._value}}); text-align:center"><a  style="color: black" target="_new">{{rendered_value }} </a></div>
    {% else %}
    <div style="background-color: rgba(255,0,0,{{d180_scaled._value}}); text-align:center"><a  style="color: black" target="_new">{{rendered_value }} </a></div>

    {% endif %}

    ;;
  }

  measure: d180_scaled {
    type:  number
    value_format: "0.00%"
    sql:   case when SUM(${cost}) != 0 and SUM( ${d0_d180_exp_ltv} ) / SUM(${cost}) >= 1 then (SUM( ${d0_d180_exp_ltv} ) / SUM(${cost}))/3
            when SUM(${cost}) != 0 and SUM( ${d0_d180_exp_ltv} ) / SUM(${cost}) >= 0.85 then (SUM( ${d0_d180_exp_ltv} ) / SUM(${cost}))/1
            when SUM(${cost}) != 0 and SUM( ${d0_d180_exp_ltv} ) / SUM(${cost}) < 0.85 then ((SUM( ${d0_d180_exp_ltv} ) / SUM(${cost}))-0.85)/(0.7-0.85)
            else null end;;
  }

  measure: d180_exp_roas {
    type:  number
    value_format: "0.00%"
    sql:    case when MIN(${day_diff}) <= 0 and SUM(${cost}) != 0 THEN (SUM( ${d0_d180_exp_ltv} ) / SUM(${cost}))
    when MIN(${day_diff}) < 3 and SUM(${cost}) != 0 THEN (SUM( ${d1_d180_exp_ltv} ) / SUM(${cost}))
    when MIN(${day_diff}) <7 and SUM(${cost}) != 0 THEN (SUM( ${d3_d180_exp_ltv} ) / SUM(${cost}))
    when MIN(${day_diff}) >= 7 and SUM(${cost}) != 0 THEN (SUM( ${d7_d180_exp_ltv} ) / SUM(${cost}))  ELSE null END;;
    html:
    {% if value >= 1 %}

    <div style="background-color: rgba(0,210,15,{{d180_scaled._value}}); text-align:center"><a  style="color: black" target="_new">{{rendered_value }} </a></div>
    {% elsif value >= 0.85 %}
    <div style="background-color: rgba(250,240,53,{{d180_scaled._value}}); text-align:center"><a  style="color: black" target="_new">{{rendered_value }} </a></div>
    {% else %}
    <div style="background-color: rgba(255,0,0,{{d180_scaled._value}}); text-align:center"><a  style="color: black" target="_new">{{rendered_value }} </a></div>

    {% endif %}

    ;; }



}
