view: ad_revenue_unpivoted {
  derived_table: {
    sql: with temp1 as (SELECT date,platform as operating_system,total_is_ad_rev as sum_is_ad_rev,total_rv_ad_rev as sum_rv_ad_rev,
total_bn_ad_rev as sum_bn_ad_rev, IAP as sum_total_purchase_actual FROM `mergedom-home-design.analytics_263095698.daily_revenue`)


      select * from temp1
      UNPIVOT (Revenues for Type in (sum_is_ad_rev,sum_rv_ad_rev,sum_bn_ad_rev,sum_total_purchase_actual))
      ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  dimension: date {
    type: date
    datatype: date
    sql: ${TABLE}.date ;;
  }

  dimension: operating_system {
    type: string
    sql: ${TABLE}.operating_system ;;
  }

  dimension: country {
    type: string
    sql: ${TABLE}.country ;;
  }

  dimension: revenues {
    type: number
    sql: ${TABLE}.Revenues ;;
  }

  dimension: type {
    type: string
    sql: ${TABLE}.Type ;;
  }

  set: detail {
    fields: [date, operating_system, country, revenues, type]
  }


  measure: sum_revenue {
    type: number
    sql: sum(${TABLE}.revenues) ;;
  }


}
