view: linear_model_ios_180_prediction {

  derived_table: {
    sql: SELECT *,"ios" as platform,"D1" as day_diff_str FROM `mergedom-home-design.api_connection.linear_model_ios_180_prediction`
      ;;
  }


  dimension: date {
    type: date
    sql: cast(date(${TABLE}.date) as timestamp) ;;
  }

  dimension: ad_roas_pred {
    type: number
    sql: ${TABLE}.ad_roas_pred ;;
  }

  dimension: d180_roas_pred {
    type: number
    sql: ${TABLE}.d180_roas_pred ;;
  }
  dimension: platform {
    type: string
    sql: ${TABLE}.platform ;;
  }

  dimension: day_diff_str {
    type: string
    sql: ${TABLE}.day_diff_str ;;
  }

  dimension: d1_ad_roas {
    type: number
    sql: ${TABLE}.d1_ad_roas ;;
  }

  dimension: d1_iap_roas {
    type: number
    sql: ${TABLE}.d1_iap_roas ;;
  }

  dimension: cost {
    type: number
    sql: ${TABLE}.cost ;;
  }

  dimension_group: date {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.date ;;
  }

  dimension: iap_roas_pred {
    type: number
    sql: ${TABLE}.iap_roas_pred ;;
  }

  dimension_group: run {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    datatype: datetime
    sql: ${TABLE}.run_time ;;
  }

  measure: d180_pred {
    type: number
    sql: sum(${d180_roas_pred})/100 ;;
    #html: <p style="color: black; font-size: 14px">{{total_90}} </p> ;;
  }

  measure: cost_m {
    type: number
    sql: sum(${cost}) ;;
    #html: <p style="color: black; font-size: 14px">{{total_90}} </p> ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
