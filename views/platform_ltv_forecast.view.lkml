#view: platform_ltv_forecast {
#  sql_table_name: `api_connection.platform_ltv_forecast`
#    ;;

view: platform_ltv_forecast {

  # Or, you could make this view a derived table, like this:

  derived_table: {
    sql: with temp1 as (select *,cast(install_time as string) as install_time_str,concat("D",date_diff( date(run_time), date(install_time)+2, day)) as day_diff_str, date_diff( date(run_time), date(install_time)+2, day) day_diff,
    LEAD(installs) over(partition by date_diff( date(run_time), date(install_time)+2, day),platform ORDER BY install_time desc) as lag_install,

    LEAD(dau) over(partition by date_diff( date(run_time), date(install_time)+2, day),platform ORDER BY install_time desc) as lag_dau
     FROM `mergedom-home-design.api_connection.platform_ltv_forecast` where date_diff( date(run_time), date(install_time)+2, day) >= 0)
,
temp2 as (SELECT date,
case when platform = "iOS" Then "ios" ELSE "android" END AS platform,
IAP as bq_IAP FROM `mergedom-home-design.analytics_263095698.daily_revenue` ),

temp4 as (SELECT date,platform,sum(spend) as spend FROM `bigger-looker-analytics.moloco.all_apps_country_cost` where date < '2022-07-15'
GROUP BY date,platform),

temp3 as (
select temp1.* EXCEPT(total_revenue,iap_revenue,cost),temp2.bq_IAP*10/7 as iap_revenue,
temp1.ad_revenue + (temp2.bq_IAP * 10/ 7) + facebook_ad_shown_monetized as total_revenue,
temp1.cost+ifnull(temp4.spend,0) as cost  from temp1 left join temp2
on cast(temp1.install_time as date) = temp2.date and temp1.platform = temp2.platform
left join temp4
on cast(temp1.install_time as date) = temp4.date and temp1.platform = temp4.platform)



select *,
LEAD(total_revenue) over(partition by date_diff( date(run_time), date(install_time)+2, day),platform ORDER BY install_time desc) as lag_total_revenue,
    LEAD(iap_revenue) over(partition by date_diff( date(run_time), date(install_time)+2, day),platform ORDER BY install_time desc) as lag_iap_revenue,
    LEAD(cost) over(partition by date_diff( date(run_time), date(install_time)+2, day),platform ORDER BY install_time desc) as lag_cost,
    LEAD(revenue_sum_day_0) over(partition by date_diff( date(run_time), date(install_time)+2, day),platform ORDER BY install_time desc) as lag_revenue_sum_day_0,
    LEAD(af_purchase_sum_day_0) over(partition by date_diff( date(run_time), date(install_time)+2, day),platform ORDER BY install_time desc) as lag_af_purchase_sum_day_0,
    LEAD(revenue_sum_day_1) over(partition by date_diff( date(run_time), date(install_time)+2, day),platform ORDER BY install_time desc) as lag_revenue_sum_day_1,
    LEAD(af_purchase_sum_day_1) over(partition by date_diff( date(run_time), date(install_time)+2, day),platform ORDER BY install_time desc) as lag_af_purchase_sum_day_1,
    LEAD(revenue_sum_day_3) over(partition by date_diff( date(run_time), date(install_time)+2, day),platform ORDER BY install_time desc) as lag_revenue_sum_day_3,
    LEAD(af_purchase_sum_day_3) over(partition by date_diff( date(run_time), date(install_time)+2, day),platform ORDER BY install_time desc) as lag_af_purchase_sum_day_3,
    LEAD(revenue_sum_day_7) over(partition by date_diff( date(run_time), date(install_time)+2, day),platform ORDER BY install_time desc) as lag_revenue_sum_day_7,
    LEAD(af_purchase_sum_day_7) over(partition by date_diff( date(run_time), date(install_time)+2, day),platform ORDER BY install_time desc) as lag_af_purchase_sum_day_7,
    'Total' AS total
 from temp3



    ;;


  }


  dimension: ad_revenue {
    type: number
    sql: ${TABLE}.ad_revenue ;;
  }

  dimension: bq_IAP {
    type: number
    sql: ${TABLE}.bq_IAP ;;
  }

  dimension: install_time_str {
    type: string
    sql: ${TABLE}.install_time_str ;;
  }

  dimension: title {
    type: string
    sql: ${day_diff_str} ;;
    html: <h4>{{rendered_value}} ROAS</h4> ;;
  }

  dimension: lag_revenue_sum_day_0 {
    type: number
    sql: ${TABLE}.lag_revenue_sum_day_0 ;;
  }

  dimension: lag_af_purchase_sum_day_0 {
    type: number
    sql: ${TABLE}.lag_af_purchase_sum_day_0 ;;
  }

  dimension: lag_revenue_sum_day_1 {
    type: number
    sql: ${TABLE}.lag_revenue_sum_day_1 ;;
  }

  dimension: lag_af_purchase_sum_day_1 {
    type: number
    sql: ${TABLE}.lag_af_purchase_sum_day_1 ;;
  }

  dimension: lag_revenue_sum_day_3 {
    type: number
    sql: ${TABLE}.lag_revenue_sum_day_3 ;;
  }

  dimension: lag_af_purchase_sum_day_3 {
    type: number
    sql: ${TABLE}.lag_af_purchase_sum_day_3 ;;
  }

  dimension: lag_revenue_sum_day_7 {
    type: number
    sql: ${TABLE}.lag_revenue_sum_day_7 ;;
  }

  dimension: lag_af_purchase_sum_day_7 {
    type: number
    sql: ${TABLE}.lag_af_purchase_sum_day_7 ;;
  }


  dimension: af_purchase_sum_day_0 {
    type: number
    sql: ${TABLE}.af_purchase_sum_day_0 ;;
  }

  dimension: day_diff {
    type: number
    sql: ${TABLE}.day_diff ;;
  }

  dimension: lag_total_revenue {
    type: number
    sql: ${TABLE}.lag_total_revenue ;;
  }

  dimension: lag_iap_revenue {
    type: number
    sql: ${TABLE}.lag_iap_revenue ;;
  }

  dimension: lag_dau {
    type: number
    sql: ${TABLE}.lag_dau ;;
  }

  dimension: lag_install {
    type: number
    sql: ${TABLE}.lag_install ;;
  }

  dimension: lag_cost {
    type: number
    sql: ${TABLE}.lag_cost ;;
  }

  dimension: day_diff_str {
    type:  string
    sql:${TABLE}.day_diff_str;;
  }


  dimension: af_purchase_sum_day_1 {
    type: number
    sql: ${TABLE}.af_purchase_sum_day_1 ;;
  }

  dimension: af_purchase_sum_day_3 {
    type: number
    sql: ${TABLE}.af_purchase_sum_day_3 ;;
  }

  dimension: af_purchase_sum_day_7 {
    type: number
    sql: ${TABLE}.af_purchase_sum_day_7 ;;
  }

  dimension: app_name {
    type: string
    sql: ${TABLE}.app_name ;;
  }

  dimension: app_opened_monetized {
    type: number
    sql: ${TABLE}.app_opened_monetized ;;
  }

  dimension: cost {
    type: number
    sql: ${TABLE}.cost ;;
  }

  dimension: d0_d120_exp_ltv {
    type: number
    sql: ${TABLE}.d0_d120_exp_ltv ;;
  }

  dimension: d0_d120_exp_ltv_app {
    type: number
    sql: ${TABLE}.d0_d120_exp_ltv_app ;;
  }

  dimension: d0_d14_exp_ltv {
    type: number
    sql: ${TABLE}.d0_d14_exp_ltv ;;
  }

  dimension: d0_d14_exp_ltv_app {
    type: number
    sql: ${TABLE}.d0_d14_exp_ltv_app ;;
  }

  dimension: d0_d180_exp_ltv {
    type: number
    sql: ${TABLE}.d0_d180_exp_ltv ;;
  }

  dimension: d0_d180_exp_ltv_app {
    type: number
    sql: ${TABLE}.d0_d180_exp_ltv_app ;;
  }

  dimension: d0_d270_exp_ltv {
    type: number
    sql: ${TABLE}.d0_d270_exp_ltv ;;
  }

  dimension: d0_d270_exp_ltv_app {
    type: number
    sql: ${TABLE}.d0_d270_exp_ltv_app ;;
  }

  dimension: d0_d30_exp_ltv {
    type: number
    sql: ${TABLE}.d0_d30_exp_ltv ;;
  }

  dimension: d0_d30_exp_ltv_app {
    type: number
    sql: ${TABLE}.d0_d30_exp_ltv_app ;;
  }

  dimension: d0_d360_exp_ltv {
    type: number
    sql: ${TABLE}.d0_d360_exp_ltv ;;
  }

  dimension: d0_d360_exp_ltv_app {
    type: number
    sql: ${TABLE}.d0_d360_exp_ltv_app ;;
  }

  dimension: d0_d3_exp_ltv {
    type: number
    sql: ${TABLE}.d0_d3_exp_ltv ;;
  }

  dimension: d0_d3_exp_ltv_app {
    type: number
    sql: ${TABLE}.d0_d3_exp_ltv_app ;;
  }

  dimension: d0_d60_exp_ltv {
    type: number
    sql: ${TABLE}.d0_d60_exp_ltv ;;
  }

  dimension: d0_d60_exp_ltv_app {
    type: number
    sql: ${TABLE}.d0_d60_exp_ltv_app ;;
  }

  dimension: d0_d720_exp_ltv {
    type: number
    sql: ${TABLE}.d0_d720_exp_ltv ;;
  }

  dimension: d0_d720_exp_ltv_app {
    type: number
    sql: ${TABLE}.d0_d720_exp_ltv_app ;;
  }

  dimension: d0_d7_exp_ltv {
    type: number
    sql: ${TABLE}.d0_d7_exp_ltv ;;
  }

  dimension: d0_d7_exp_ltv_app {
    type: number
    sql: ${TABLE}.d0_d7_exp_ltv_app ;;
  }

  dimension: d0_d90_exp_ltv {
    type: number
    sql: ${TABLE}.d0_d90_exp_ltv ;;
  }

  dimension: d0_d90_exp_ltv_app {
    type: number
    sql: ${TABLE}.d0_d90_exp_ltv_app ;;
  }

  dimension: d13_iap_rev {
    type: number
    sql: ${TABLE}.d13_iap_rev ;;
  }

  dimension: d13_rev {
    type: number
    sql: ${TABLE}.d13_rev ;;
  }

  dimension: d1_d120_exp_ltv {
    type: number
    sql: ${TABLE}.d1_d120_exp_ltv ;;
  }

  dimension: d1_d120_exp_ltv_app {
    type: number
    sql: ${TABLE}.d1_d120_exp_ltv_app ;;
  }

  dimension: d1_d14_exp_ltv {
    type: number
    sql: ${TABLE}.d1_d14_exp_ltv ;;
  }

  dimension: d1_d14_exp_ltv_app {
    type: number
    sql: ${TABLE}.d1_d14_exp_ltv_app ;;
  }

  dimension: d1_d180_exp_ltv {
    type: number
    sql: ${TABLE}.d1_d180_exp_ltv ;;
  }

  dimension: d1_d180_exp_ltv_app {
    type: number
    sql: ${TABLE}.d1_d180_exp_ltv_app ;;
  }

  dimension: d1_d270_exp_ltv {
    type: number
    sql: ${TABLE}.d1_d270_exp_ltv ;;
  }

  dimension: d1_d270_exp_ltv_app {
    type: number
    sql: ${TABLE}.d1_d270_exp_ltv_app ;;
  }

  dimension: d1_d30_exp_ltv {
    type: number
    sql: ${TABLE}.d1_d30_exp_ltv ;;
  }

  dimension: d1_d30_exp_ltv_app {
    type: number
    sql: ${TABLE}.d1_d30_exp_ltv_app ;;
  }

  dimension: d1_d360_exp_ltv {
    type: number
    sql: ${TABLE}.d1_d360_exp_ltv ;;
  }

  dimension: d1_d360_exp_ltv_app {
    type: number
    sql: ${TABLE}.d1_d360_exp_ltv_app ;;
  }

  dimension: d1_d3_exp_ltv {
    type: number
    sql: ${TABLE}.d1_d3_exp_ltv ;;
  }

  dimension: d1_d3_exp_ltv_app {
    type: number
    sql: ${TABLE}.d1_d3_exp_ltv_app ;;
  }

  dimension: d1_d60_exp_ltv {
    type: number
    sql: ${TABLE}.d1_d60_exp_ltv ;;
  }

  dimension: d1_d60_exp_ltv_app {
    type: number
    sql: ${TABLE}.d1_d60_exp_ltv_app ;;
  }

  dimension: d1_d720_exp_ltv {
    type: number
    sql: ${TABLE}.d1_d720_exp_ltv ;;
  }

  dimension: d1_d720_exp_ltv_app {
    type: number
    sql: ${TABLE}.d1_d720_exp_ltv_app ;;
  }

  dimension: d1_d7_exp_ltv {
    type: number
    sql: ${TABLE}.d1_d7_exp_ltv ;;
  }

  dimension: d1_d7_exp_ltv_app {
    type: number
    sql: ${TABLE}.d1_d7_exp_ltv_app ;;
  }

  dimension: d1_d90_exp_ltv {
    type: number
    sql: ${TABLE}.d1_d90_exp_ltv ;;
  }

  dimension: d1_d90_exp_ltv_app {
    type: number
    sql: ${TABLE}.d1_d90_exp_ltv_app ;;
  }

  dimension: d3_d120_exp_ltv {
    type: number
    sql: ${TABLE}.d3_d120_exp_ltv ;;
  }

  dimension: d3_d120_exp_ltv_app {
    type: number
    sql: ${TABLE}.d3_d120_exp_ltv_app ;;
  }

  dimension: d3_d14_exp_ltv {
    type: number
    sql: ${TABLE}.d3_d14_exp_ltv ;;
  }

  dimension: d3_d14_exp_ltv_app {
    type: number
    sql: ${TABLE}.d3_d14_exp_ltv_app ;;
  }

  dimension: d3_d180_exp_ltv {
    type: number
    sql: ${TABLE}.d3_d180_exp_ltv ;;
  }

  dimension: d3_d180_exp_ltv_app {
    type: number
    sql: ${TABLE}.d3_d180_exp_ltv_app ;;
  }

  dimension: d3_d270_exp_ltv {
    type: number
    sql: ${TABLE}.d3_d270_exp_ltv ;;
  }

  dimension: d3_d270_exp_ltv_app {
    type: number
    sql: ${TABLE}.d3_d270_exp_ltv_app ;;
  }

  dimension: d3_d30_exp_ltv {
    type: number
    sql: ${TABLE}.d3_d30_exp_ltv ;;
  }

  dimension: d3_d30_exp_ltv_app {
    type: number
    sql: ${TABLE}.d3_d30_exp_ltv_app ;;
  }

  dimension: d3_d360_exp_ltv {
    type: number
    sql: ${TABLE}.d3_d360_exp_ltv ;;
  }

  dimension: d3_d360_exp_ltv_app {
    type: number
    sql: ${TABLE}.d3_d360_exp_ltv_app ;;
  }

  dimension: d3_d60_exp_ltv {
    type: number
    sql: ${TABLE}.d3_d60_exp_ltv ;;
  }

  dimension: d3_d60_exp_ltv_app {
    type: number
    sql: ${TABLE}.d3_d60_exp_ltv_app ;;
  }

  dimension: d3_d720_exp_ltv {
    type: number
    sql: ${TABLE}.d3_d720_exp_ltv ;;
  }

  dimension: d3_d720_exp_ltv_app {
    type: number
    sql: ${TABLE}.d3_d720_exp_ltv_app ;;
  }

  dimension: d3_d7_exp_ltv {
    type: number
    sql: ${TABLE}.d3_d7_exp_ltv ;;
  }

  dimension: d3_d7_exp_ltv_app {
    type: number
    sql: ${TABLE}.d3_d7_exp_ltv_app ;;
  }

  dimension: d3_d90_exp_ltv {
    type: number
    sql: ${TABLE}.d3_d90_exp_ltv ;;
  }

  dimension: d3_d90_exp_ltv_app {
    type: number
    sql: ${TABLE}.d3_d90_exp_ltv_app ;;
  }

  dimension: d7_d120_exp_ltv {
    type: number
    sql: ${TABLE}.d7_d120_exp_ltv ;;
  }

  dimension: d7_d120_exp_ltv_app {
    type: number
    sql: ${TABLE}.d7_d120_exp_ltv_app ;;
  }

  dimension: d7_d14_exp_ltv {
    type: number
    sql: ${TABLE}.d7_d14_exp_ltv ;;
  }

  dimension: d7_d14_exp_ltv_app {
    type: number
    sql: ${TABLE}.d7_d14_exp_ltv_app ;;
  }

  dimension: d7_d180_exp_ltv {
    type: number
    sql: ${TABLE}.d7_d180_exp_ltv ;;
  }

  dimension: d7_d180_exp_ltv_app {
    type: number
    sql: ${TABLE}.d7_d180_exp_ltv_app ;;
  }

  dimension: d7_d270_exp_ltv {
    type: number
    sql: ${TABLE}.d7_d270_exp_ltv ;;
  }

  dimension: d7_d270_exp_ltv_app {
    type: number
    sql: ${TABLE}.d7_d270_exp_ltv_app ;;
  }

  dimension: d7_d30_exp_ltv {
    type: number
    sql: ${TABLE}.d7_d30_exp_ltv ;;
  }

  dimension: d7_d30_exp_ltv_app {
    type: number
    sql: ${TABLE}.d7_d30_exp_ltv_app ;;
  }

  dimension: d7_d360_exp_ltv {
    type: number
    sql: ${TABLE}.d7_d360_exp_ltv ;;
  }

  dimension: d7_d360_exp_ltv_app {
    type: number
    sql: ${TABLE}.d7_d360_exp_ltv_app ;;
  }

  dimension: d7_d60_exp_ltv {
    type: number
    sql: ${TABLE}.d7_d60_exp_ltv ;;
  }

  dimension: d7_d60_exp_ltv_app {
    type: number
    sql: ${TABLE}.d7_d60_exp_ltv_app ;;
  }

  dimension: d7_d720_exp_ltv {
    type: number
    sql: ${TABLE}.d7_d720_exp_ltv ;;
  }

  dimension: d7_d720_exp_ltv_app {
    type: number
    sql: ${TABLE}.d7_d720_exp_ltv_app ;;
  }

  dimension: d7_d90_exp_ltv {
    type: number
    sql: ${TABLE}.d7_d90_exp_ltv ;;
  }

  dimension: d7_d90_exp_ltv_app {
    type: number
    sql: ${TABLE}.d7_d90_exp_ltv_app ;;
  }

  dimension: dau {
    type: number
    sql: ${TABLE}.dau ;;
  }

  dimension: facebook_ad_shown_monetized {
    type: number
    sql: ${TABLE}.facebook_ad_shown_monetized ;;
  }

  dimension: iap_revenue {
    type: number
    sql: ${TABLE}.iap_revenue ;;
  }

  dimension: install_time {

    type: string
    sql: ${TABLE}.install_time ;;

  }


  dimension: install_date {
    type: date
    sql: cast(date(${TABLE}.install_time) as timestamp) ;;
  }

  dimension: install_date_str {
    type: string
    sql:${TABLE}.install_time_str ;;
  }

  dimension: run_date_filter {
    type: date
    sql: cast(date(${TABLE}.run_time) as timestamp) ;;
  }

  dimension: is_last {
    type: string
    sql: case when date(${TABLE}.install_time)+2 = CURRENT_DATE() THEN "Yes" ELSE "No" END ;;
  }

  dimension: installs {
    type: number
    sql: ${TABLE}.installs ;;
  }

  dimension: platform {
    type: string
    sql: case when ${TABLE}.platform = "android" then "Android" Else "IOS" End  ;;
  }

  dimension: ret_d1 {
    type: number
    sql: ${TABLE}.ret_d1 ;;
  }

  dimension: revenue_sum_day_0 {
    type: number
    sql: ${TABLE}.revenue_sum_day_0 ;;
  }

  dimension: revenue_sum_day_1 {
    type: number
    sql: ${TABLE}.revenue_sum_day_1 ;;
  }

  dimension: revenue_sum_day_3 {
    type: number
    sql: ${TABLE}.revenue_sum_day_3 ;;
  }

  dimension: revenue_sum_day_7 {
    type: number
    sql: ${TABLE}.revenue_sum_day_7 ;;
  }

  dimension_group: runner {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.run_time ;;
  }

  dimension_group: run_installdate {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: cast(${TABLE}.install_date as timestamp) ;;
  }

  dimension: total_revenue {
    type: number
    sql: ${TABLE}.total_revenue    ;;
  }

  measure: total_revenue_change {
    type:  number
    value_format: "0.00\%"
    sql:  ROUND((${revenue_total}-${lag_revenue_total})*100/(${lag_revenue_total}),2) ;;
    #html: <p style="color: black; font-size: 18px">{{install_total}} </p> ;;
  }

  measure: lag_revenue_total {
    type:  number
    value_format: "$0"
    #value_format_name: usd
    sql:  round(sum( ${lag_total_revenue} ) - sum(${lag_iap_revenue})*0.3, 2) ;;
    #html: <p style="color: black; font-size: 18px">${{revenue_total}} </p> ;;
  }


  measure: installs_ios {
    type:  number
    sql:  sum(case when ${platform} = 'IOS' then ${installs} end)  ;;
  }

  measure: installs_ios_change {
    type:  number
    value_format: "0.00\%"
    sql:  ROUND((sum(case when ${platform} = 'IOS' then ${installs} end)-sum(case when ${platform} = 'IOS' then ${lag_install} end))*100/sum(case when ${platform} = 'IOS' then ${lag_install} end),2)  ;;
  }

  measure: installs_android {
    type:  number
    sql:  sum(case when ${platform} = 'Android' then ${installs} end)  ;;
  }

  measure: installs_android_change {
    type:  number
    value_format: "0.00\%"
    sql:  ROUND((sum(case when ${platform} = 'Android' then ${installs} end)-sum(case when ${platform} = 'Android' then ${lag_install} end))*100/sum(case when ${platform} = 'Android' then ${lag_install} end),2)  ;;
  }

  measure: install_total {
    type:  number
    sql:  SUM(${installs}) ;;
    #html: <p style="color: black; font-size: 18px">{{install_total}} </p> ;;
  }

  measure: install_total_change {
    type:  number
    value_format: "0.00\%"
    sql:  (SUM(${installs})-SUM(${lag_install}))*100/SUM(${lag_install}) ;;
    #html: <p style="color: black; font-size: 18px">{{install_total}} </p> ;;
  }

  measure: dau_total_change {
    type:  number
    value_format: "0.00\%"
    sql:  (SUM(${dau})-SUM(${lag_dau}))*100/SUM(${lag_dau}) ;;
    #html: <p style="color: black; font-size: 18px">{{install_total}} </p> ;;
  }

  measure: cost_total_change {
    type:  number
    value_format: "0.00\%"
    sql:  (SUM(${cost})-SUM(${lag_cost}))*100/SUM(${lag_cost}) ;;
    #html: <p style="color: black; font-size: 18px">{{install_total}} </p> ;;
  }

  measure: cpi_total_change {
    type:  number
    value_format: "0.00\%"
    sql:  round(((sum(${cost})/sum(${installs})) - (sum(${lag_cost})/sum(${lag_install})))*100/(sum(${lag_cost})/sum(${lag_install})),2);;
    #html: <p style="color: black; font-size: 18px">{{install_total}} </p> ;;
  }


  measure: install_total_str {
    type:  number
    sql:  format("%'d", sum(${installs})) ;;
    html: <p style="color: black; font-size: 18px">{{install_total_str}} </p> ;;
  }

  measure: install_ios_str {
    type:  number
    sql:  format("%'d", sum(case when ${platform} = 'IOS' then ${installs} end)) ;;
    html: <p style="color: black; font-size: 18px">{{install_ios_str}} </p> ;;
  }

  measure: install_android_str {
    type:  number
    sql:  format("%'d", sum(case when ${platform} = 'Android' then ${installs} end)) ;;
    html: <p style="color: black; font-size: 18px">{{install_android_str}} </p> ;;
  }

  measure: cost_ios {
    type:  number
    value_format_name: usd
    sql:  sum(case when ${platform} = 'IOS' then ${cost} end)  ;;
  }
  measure: lag_cost_ios {
    type:  number
    value_format_name: usd
    sql:  sum(case when ${platform} = 'IOS' then ${lag_cost} end)  ;;
  }

  measure: lag_cost_android {
    type:  number
    value_format_name: usd
    sql:  sum(case when ${platform} = 'Android' then ${lag_cost} end)  ;;
  }

  measure: cost_ios_change {
    type:  number
    value_format: "0.00\%"
    sql: ROUND((${cost_ios}-${lag_cost_ios})*100/${lag_cost_ios},2)  ;;
  }

  measure: cost_android_change {
    type:  number
    value_format: "0.00\%"
    sql: ROUND((${cost_android}-${lag_cost_android})*100/${lag_cost_android},2)  ;;
  }


  measure: cost_android {
    type:  number
    value_format_name: usd
    sql:  sum(case when ${platform} = 'Android' then ${cost} end)  ;;
  }

  measure: cost_total {
    type:  number
    value_format_name: usd
    sql:  sum(${cost})  ;;
    #html: <p style="color: black; font-size: 18px">${{cost_total}} </p> ;;
  }

  measure: cost_total_str {
    type:  number
    value_format_name: usd
    sql:  format("%'d", cast(sum(${cost}) as int) ) ;;
    html: <p style="color: black; font-size: 18px">${{cost_total_str}} </p> ;;
  }

  measure: cost_ios_str {
    type:  number
    value_format_name: usd
    sql:  format("%'d", cast(sum(case when ${platform} = 'IOS' then ${cost} end) as int) )  ;;
    html: <p style="color: black; font-size: 18px">${{cost_ios_str}} </p> ;;
  }

  measure: cost_android_str {
    type:  number
    value_format_name: usd
    sql:  format("%'d", cast(sum(case when ${platform} = 'Android' then ${cost} end)as int) )  ;;
    html: <p style="color: black; font-size: 18px">${{cost_android_str}} </p> ;;
  }

  measure: cpi_ios {
    type:  number
    value_format: "$0.00"
    sql:  round(sum(case when ${platform} = 'IOS' then ${cost} end) / sum(case when ${platform} = 'IOS' then ${installs} end), 2) ;;
    html: <p style="color: black; font-size: 18px">${{cpi_ios}} </p> ;;
  }

  measure: lag_cpi_ios {
    type:  number
    value_format: "$0.00"
    sql:  round(sum(case when ${platform} = 'IOS' then ${lag_cost} end) / sum(case when ${platform} = 'IOS' then ${lag_install} end), 2) ;;
    html: <p style="color: black; font-size: 18px">${{cpi_ios}} </p> ;;
  }

  measure: lag_cpi_android {
    type:  number
    value_format: "$0.00"
    sql:  round(sum(case when ${platform} = 'Android' then ${lag_cost} end) / sum(case when ${platform} = 'Android' then ${lag_install} end), 2) ;;
    html: <p style="color: black; font-size: 18px">${{cpi_ios}} </p> ;;
  }

  measure: cpi_ios_change {
    type:  number
    value_format: "0.00\%"
    sql: ROUND((${cpi_ios}-${lag_cpi_ios})*100/${lag_cpi_ios},2)  ;;
  }

  measure: cpi_android_change {
    type:  number
    value_format: "0.00\%"
    sql: ROUND((${cpi_android}-${lag_cpi_android})*100/${lag_cpi_android},2)  ;;
  }


  measure: cpi_android {
    type:  number
    value_format: "$0.00"
    sql:  round(sum(case when ${platform} = 'Android' then ${cost} end) / sum(case when ${platform} = 'Android' then ${installs} end), 2) ;;
    html: <p style="color: black; font-size: 18px">${{cpi_android}} </p> ;;
  }

  measure: cpi_total {
    type:  number
    value_format: "$0.00"
    sql:  round(sum(${cost}) / sum(${installs}), 2) ;;
    html: <p style="color: black; font-size: 18px">${{cpi_total}} </p> ;;
  }

  measure: revenue_ios {
    type:  number
    value_format: "$0"
    sql:  sum(CASE WHEN ${platform} = 'IOS' THEN ${total_revenue} END) - sum(CASE WHEN ${platform} = 'IOS' THEN ${iap_revenue} END)*0.3 ;;
  }

  measure: lag_revenue_ios {
    type:  number
    value_format: "$0"
    sql:  sum(CASE WHEN ${platform} = 'IOS' THEN ${lag_total_revenue} END) - sum(CASE WHEN ${platform} = 'IOS' THEN ${lag_iap_revenue} END)*0.3 ;;
  }

  measure: lag_revenue_android {
    type:  number
    value_format: "$0"
    sql:  sum(CASE WHEN ${platform} = 'Android' THEN ${lag_total_revenue} END) - sum(CASE WHEN ${platform} = 'Android' THEN ${lag_iap_revenue} END)*0.3 ;;
  }



  measure: revenue_ios_change {
    type:  number
    value_format: "0.00\%"
    sql: ROUND((${revenue_ios}-${lag_revenue_ios})*100/${lag_revenue_ios},2)  ;;
  }

  measure: revenue_android_change {
    type:  number
    value_format: "0.00\%"
    sql: ROUND((${revenue_android}-${lag_revenue_android})*100/${lag_revenue_android},2)  ;;
  }


  measure: revenue_android {
    type:  number
    value_format: "$0"
    sql:  sum(CASE WHEN ${platform} = 'Android' THEN ${total_revenue} END) - sum(CASE WHEN ${platform} = 'Android' THEN ${iap_revenue} END)*0.3 ;;
  }

  measure: revenue_total {
    type:  number
    value_format: "$0"
    #value_format_name: usd
    sql:  round(sum( ${total_revenue} ) - sum(${iap_revenue})*0.3, 2) ;;
    #html: <p style="color: black; font-size: 18px">${{revenue_total}} </p> ;;
  }

  measure: revenue_total_str {
    type:  number
    value_format: "$0"
    #value_format_name: usd
    sql:  format("%'d", cast(sum( ${total_revenue} ) - sum(${iap_revenue})*0.3 as int)) ;;
    html: <p style="color: black; font-size: 18px">${{revenue_total_str}} </p> ;;
  }

  measure: revenue_android_str {
    type:  number
    value_format: "$0"
    sql:  format("%'d", cast(sum(CASE WHEN ${platform} = 'Android' THEN ${total_revenue} END) - sum(CASE WHEN ${platform} = 'Android' THEN ${iap_revenue} END)*0.3 as int))  ;;
    html: <p style="color: black; font-size: 18px">${{revenue_android_str}} </p> ;;
  }

  measure: revenue_ios_str {
    type:  number
    value_format: "$0"
    sql:  format("%'d", cast(sum(CASE WHEN ${platform} = 'IOS' THEN ${total_revenue} END) - sum(CASE WHEN ${platform} = 'IOS' THEN ${iap_revenue} END)*0.3 as int))  ;;
    html: <p style="color: black; font-size: 18px">${{revenue_ios_str}} </p> ;;
  }


  measure: dau_ios {
    type:  number
    sql:  SUM(CASE WHEN ${platform} = 'IOS' THEN ${dau} END) ;;
  }

  measure: lag_dau_ios {
    type:  number
    sql:  SUM(CASE WHEN ${platform} = 'IOS' THEN ${lag_dau} END) ;;
  }

  measure: lag_dau_android {
    type:  number
    sql:  SUM(CASE WHEN ${platform} = 'Android' THEN ${lag_dau} END) ;;
  }

  measure: dau_ios_change {
    type:  number
    value_format: "0.00\%"
    sql: ROUND((${dau_ios}-${lag_dau_ios})*100/${lag_dau_ios},2)  ;;
  }

  measure: dau_android_change {
    type:  number
    value_format: "0.00\%"
    sql: ROUND((${dau_android}-${lag_dau_android})*100/${lag_dau_android},2)  ;;
  }

  measure: dau_android {
    type:  number
    sql:  SUM(CASE WHEN ${platform} = 'Android' THEN ${dau} END) ;;
  }

  measure: dau_total {
    type:  number
    sql:  SUM(${dau}) ;;
    #html: <p style="color: black; font-size: 18px">${{dau_total}} </p> ;;
  }

  measure: dau_total_str {
    type:  number
    sql:  format("%'d", cast(SUM(${dau}) as int)) ;;
    html: <p style="color: black; font-size: 18px">{{dau_total_str}} </p> ;;
  }

  measure: dau_android_str {
    type:  number
    sql:  format("%'d", cast(SUM(CASE WHEN ${platform} = 'Android' THEN ${dau} END) as int));;
    html: <p style="color: black; font-size: 18px">{{dau_android_str}} </p> ;;
  }

  measure: dau_ios_str {
    type:  number
    sql:  format("%'d", cast(SUM(CASE WHEN ${platform} = 'IOS' THEN ${dau} END) as int));;
    html: <p style="color: black; font-size: 18px">{{dau_ios_str}} </p> ;;
  }
  measure: arpdau_ios {
    type:  number
    value_format: "$0.00"
    sql:  (SUM(CASE WHEN ${platform} = 'IOS' THEN ${total_revenue} END) - SUM(CASE WHEN ${platform} = 'IOS' THEN ${iap_revenue} END)*0.3) / SUM(CASE WHEN ${platform} = 'IOS' THEN ${dau} END) ;;

  }

  measure: lag_arpdau_ios {
    type:  number
    value_format: "$0.00"
    sql:  (SUM(CASE WHEN ${platform} = 'IOS' THEN ${lag_total_revenue} END) - SUM(CASE WHEN ${platform} = 'IOS' THEN ${lag_iap_revenue} END)*0.3) / SUM(CASE WHEN ${platform} = 'IOS' THEN ${lag_dau} END) ;;

  }

  measure: lag_arpdau_android {
    type:  number
    value_format: "$0.00"
    sql:  (SUM(CASE WHEN ${platform} = 'Android' THEN ${lag_total_revenue} END) - SUM(CASE WHEN ${platform} = 'Android' THEN ${lag_iap_revenue} END)*0.3) / SUM(CASE WHEN ${platform} = 'Android' THEN ${lag_dau} END) ;;

  }


  measure: arpdau_ios_change {
    type:  number
    value_format: "0.00\%"
    sql: ROUND((${arpdau_ios}-${lag_arpdau_ios})*100/${lag_arpdau_ios},2)  ;;
  }

  measure: arpdau_android_change {
    type:  number
    value_format: "0.00\%"
    sql: ROUND((${arpdau_android}-${lag_arpdau_android})*100/${lag_arpdau_android},2)  ;;
  }

  measure: arpdau_total_change {
    type:  number
    value_format: "0.00\%"
    sql:  (${arpdau_total}-${lag_arpdau_total})*100/(${lag_arpdau_total}) ;;
    #html: <p style="color: black; font-size: 18px">{{install_total}} </p> ;;
  }

  measure: arpdau_android {
    type:  number
    value_format: "$0.00"
    sql:  (SUM(CASE WHEN ${platform} = 'Android' THEN ${total_revenue} END) - SUM(CASE WHEN ${platform} = 'Android' THEN ${iap_revenue} END)*0.3) / SUM(CASE WHEN ${platform} = 'Android' THEN ${dau} END) ;;

  }

  measure: arpdau_ios_str {
    type:  number
    value_format: "$0.00"
    sql:  round(((SUM(CASE WHEN ${platform} = 'IOS' THEN ${total_revenue} END) - SUM(CASE WHEN ${platform} = 'IOS' THEN ${iap_revenue} END)*0.3) / SUM(CASE WHEN ${platform} = 'IOS' THEN ${dau} END)), 2) ;;
    html: <p style="color: black; font-size: 18px">${{arpdau_ios_str}} </p> ;;
  }

  measure: arpdau_android_str {
    type:  number
    value_format: "$0.00"
    sql:  round(((SUM(CASE WHEN ${platform} = 'Android' THEN ${total_revenue} END) - SUM(CASE WHEN ${platform} = 'Android' THEN ${iap_revenue} END)*0.3) / SUM(CASE WHEN ${platform} = 'Android' THEN ${dau} END)), 2) ;;
    html: <p style="color: black; font-size: 18px">${{arpdau_android_str}} </p> ;;
  }

  measure: arpdau_total {
    type:  number
    value_format: "$0.00"
    sql:  (SUM(${total_revenue}) - SUM( ${iap_revenue})*0.3) / SUM(${dau}) ;;
    html: <p style="color: black; font-size: 18px">${{arpdau_total}} </p> ;;
  }

  measure: lag_arpdau_total {
    type:  number
    value_format: "$0.00"
    sql:  (SUM(${lag_total_revenue}) - SUM( ${lag_iap_revenue})*0.3) / SUM(${lag_dau}) ;;
    html: <p style="color: black; font-size: 18px">${{arpdau_total}} </p> ;;
  }

  measure: arpdau_total_str {
    type:  number
    value_format: "$0.00"
    sql:  round((SUM(${total_revenue}) - SUM( ${iap_revenue})*0.3) / SUM(${dau}), 2) ;;
    html: <p style="color: black; font-size: 18px">${{arpdau_total_str}} </p> ;;
  }

  measure: d0_roas_ios {
    type:  number
    value_format: "0.00%"
    sql:  case when MIN(${day_diff}) <= 0 and SUM(${cost}) != 0 THEN round(((SUM(CASE WHEN  ${platform} = 'IOS' THEN ${revenue_sum_day_0} END) - SUM(CASE WHEN  ${platform} = 'IOS' THEN ${af_purchase_sum_day_0} END)*0.3) / SUM(CASE WHEN  ${platform} = 'IOS' THEN ${cost} END)) * 100, 2)
    when MIN(${day_diff}) < 3 and SUM(${cost}) != 0 THEN round(((SUM(CASE WHEN  ${platform} = 'IOS' THEN ${revenue_sum_day_1} END) - SUM(CASE WHEN  ${platform} = 'IOS' THEN ${af_purchase_sum_day_1} END)*0.3) / SUM(CASE WHEN  ${platform} = 'IOS' THEN ${cost} END)) * 100, 2)
    when MIN(${day_diff}) <7 and SUM(${cost}) != 0 THEN round(((SUM(CASE WHEN  ${platform} = 'IOS' THEN ${revenue_sum_day_3} END) - SUM(CASE WHEN  ${platform} = 'IOS' THEN ${af_purchase_sum_day_3} END)*0.3) / SUM(CASE WHEN  ${platform} = 'IOS' THEN ${cost} END)) * 100, 2)
   when MIN(${day_diff}) >= 7 and SUM(${cost}) != 0 THEN round(((SUM(CASE WHEN  ${platform} = 'IOS' THEN ${revenue_sum_day_7} END) - SUM(CASE WHEN  ${platform} = 'IOS' THEN ${af_purchase_sum_day_7} END)*0.3) / SUM(CASE WHEN  ${platform} = 'IOS' THEN ${cost} END)) * 100, 2) END

    ;;

    html: <p style="color: black; font-size: 18px">{{d0_roas_ios}}% </p> ;;
  }

  measure: lag_d0_roas_ios {
    type:  number
    value_format: "0.00%"
    sql:  case when MIN(${day_diff}) <= 0 and SUM(${cost}) != 0 THEN round(((SUM(CASE WHEN  ${platform} = 'IOS' THEN ${lag_revenue_sum_day_0} END) - SUM(CASE WHEN  ${platform} = 'IOS' THEN ${lag_af_purchase_sum_day_0} END)*0.3) / SUM(CASE WHEN  ${platform} = 'IOS' THEN ${lag_cost} END)) * 100, 2)
    when MIN(${day_diff}) < 3 and SUM(${cost}) != 0 THEN round(((SUM(CASE WHEN  ${platform} = 'IOS' THEN ${lag_revenue_sum_day_1} END) - SUM(CASE WHEN  ${platform} = 'IOS' THEN ${lag_af_purchase_sum_day_1} END)*0.3) / SUM(CASE WHEN  ${platform} = 'IOS' THEN ${lag_cost} END)) * 100, 2)
    when MIN(${day_diff}) <7 and SUM(${cost}) != 0 THEN round(((SUM(CASE WHEN  ${platform} = 'IOS' THEN ${lag_revenue_sum_day_3} END) - SUM(CASE WHEN  ${platform} = 'IOS' THEN ${lag_af_purchase_sum_day_3} END)*0.3) / SUM(CASE WHEN  ${platform} = 'IOS' THEN ${lag_cost} END)) * 100, 2)
   when MIN(${day_diff}) >= 7 and SUM(${cost}) != 0 THEN round(((SUM(CASE WHEN  ${platform} = 'IOS' THEN ${lag_revenue_sum_day_7} END) - SUM(CASE WHEN  ${platform} = 'IOS' THEN ${lag_af_purchase_sum_day_7} END)*0.3) / SUM(CASE WHEN  ${platform} = 'IOS' THEN ${lag_cost} END)) * 100, 2) END
    ;;

    html: <p style="color: black; font-size: 18px">{{lag_d0_roas_ios}}% </p> ;;
  }

  measure: lag_d0_roas_android {
    type:  number
    value_format: "0.00%"
    sql:  case when MIN(${day_diff}) <= 0 and SUM(${cost}) != 0 THEN round(((SUM(CASE WHEN  ${platform} = 'Android' THEN ${lag_revenue_sum_day_0} END) - SUM(CASE WHEN  ${platform} = 'Android' THEN ${lag_af_purchase_sum_day_0} END)*0.3) / SUM(CASE WHEN  ${platform} = 'Android' THEN ${lag_cost} END)) * 100, 2)
    when MIN(${day_diff}) < 3 and SUM(${cost}) != 0 THEN round(((SUM(CASE WHEN  ${platform} = 'Android' THEN ${lag_revenue_sum_day_1} END) - SUM(CASE WHEN  ${platform} = 'Android' THEN ${lag_af_purchase_sum_day_1} END)*0.3) / SUM(CASE WHEN  ${platform} = 'Android' THEN ${lag_cost} END)) * 100, 2)
    when MIN(${day_diff}) <7 and SUM(${cost}) != 0 THEN round(((SUM(CASE WHEN  ${platform} = 'Android' THEN ${lag_revenue_sum_day_3} END) - SUM(CASE WHEN  ${platform} = 'Android' THEN ${lag_af_purchase_sum_day_3} END)*0.3) / SUM(CASE WHEN  ${platform} = 'Android' THEN ${lag_cost} END)) * 100, 2)
   when MIN(${day_diff}) >= 7 and SUM(${cost}) != 0 THEN round(((SUM(CASE WHEN  ${platform} = 'Android' THEN ${lag_revenue_sum_day_7} END) - SUM(CASE WHEN  ${platform} = 'Android' THEN ${lag_af_purchase_sum_day_7} END)*0.3) / SUM(CASE WHEN  ${platform} = 'Android' THEN ${lag_cost} END)) * 100, 2) END

    ;;
    html: <p style="color: black; font-size: 18px">{{lag_d0_roas_android}}% </p> ;;
  }


  measure: d0roas_ios_change {
    type:  number
    value_format: "0.00\%"
    sql:  (${d0_roas_ios}-${lag_d0_roas_ios})*100/${lag_d0_roas_ios} ;;

  }

  measure: d0roas_android_change {
    type:  number
    value_format: "0.00\%"
    sql:  (${d0_roas_android}-${lag_d0_roas_android})*100/${lag_d0_roas_android} ;;

  }


  measure: d0_roas_android {
    type:  number
    value_format: "0.00%"
    sql:  case when MIN(${day_diff}) <= 0 and SUM(${cost}) != 0 THEN round(((SUM(CASE WHEN  ${platform} = 'Android' THEN ${revenue_sum_day_0} END) - SUM(CASE WHEN  ${platform} = 'Android' THEN ${af_purchase_sum_day_0} END)*0.3) / SUM(CASE WHEN  ${platform} = 'Android' THEN ${cost} END)) * 100, 2)
    when MIN(${day_diff}) < 3 and SUM(${cost}) != 0 THEN round(((SUM(CASE WHEN  ${platform} = 'Android' THEN ${revenue_sum_day_1} END) - SUM(CASE WHEN  ${platform} = 'Android' THEN ${af_purchase_sum_day_1} END)*0.3) / SUM(CASE WHEN  ${platform} = 'Android' THEN ${cost} END)) * 100, 2)
    when MIN(${day_diff}) <7 and SUM(${cost}) != 0 THEN round(((SUM(CASE WHEN  ${platform} = 'Android' THEN ${revenue_sum_day_3} END) - SUM(CASE WHEN  ${platform} = 'Android' THEN ${af_purchase_sum_day_3} END)*0.3) / SUM(CASE WHEN  ${platform} = 'Android' THEN ${cost} END)) * 100, 2)
   when MIN(${day_diff}) >= 7 and SUM(${cost}) != 0 THEN round(((SUM(CASE WHEN  ${platform} = 'Android' THEN ${revenue_sum_day_7} END) - SUM(CASE WHEN  ${platform} = 'Android' THEN ${af_purchase_sum_day_7} END)*0.3) / SUM(CASE WHEN  ${platform} = 'Android' THEN ${cost} END)) * 100, 2) END

    ;;
    html: <p style="color: black; font-size: 18px">{{d0_roas_android}}% </p> ;;
  }

  measure: d0_roas {
    type:  number
    value_format: "0.00%"
    sql:  case when MIN(${day_diff}) <= 0 and SUM(${cost}) != 0 THEN round(((SUM( ${revenue_sum_day_0} ) - SUM( ${af_purchase_sum_day_0} )*0.3) / SUM(${cost})) * 100, 2)
    when MIN(${day_diff}) < 3 and SUM(${cost}) != 0 THEN round(((SUM( ${revenue_sum_day_1} ) - SUM( ${af_purchase_sum_day_1} )*0.3) / SUM(${cost})) * 100, 2)
    when MIN(${day_diff}) <7 and SUM(${cost}) != 0 THEN round(((SUM( ${revenue_sum_day_3} ) - SUM( ${af_purchase_sum_day_3} )*0.3) / SUM(${cost})) * 100, 2)
    when MIN(${day_diff}) >= 7 and SUM(${cost}) != 0 THEN round(((SUM( ${revenue_sum_day_7} ) - SUM( ${af_purchase_sum_day_7} )*0.3) / SUM(${cost})) * 100, 2)   ELSE null END

     ;;
    html: <p style="color: black; font-size: 18px">{{d0_roas}}% </p> ;;
  }
  #round(((SUM( ${revenue_sum_day_0} ) - SUM( ${af_purchase_sum_day_0} )*0.3) / SUM(${cost})) * 100, 2)

  measure: lag_d0_roas {
    type:  number
    value_format: "0.00%"
    sql:  case when MIN(${day_diff}) <= 0 and SUM(${cost}) != 0 THEN round(((SUM( ${lag_revenue_sum_day_0} ) - SUM( ${lag_af_purchase_sum_day_0} )*0.3) / SUM(${lag_cost})) * 100, 2)
    when MIN(${day_diff}) < 3 and SUM(${cost}) != 0 THEN round(((SUM( ${lag_revenue_sum_day_1} ) - SUM( ${lag_af_purchase_sum_day_1} )*0.3) / SUM(${lag_cost})) * 100, 2)
    when MIN(${day_diff}) <7 and SUM(${cost}) != 0 THEN round(((SUM( ${lag_revenue_sum_day_3} ) - SUM( ${lag_af_purchase_sum_day_3} )*0.3) / SUM(${lag_cost})) * 100, 2)
    when MIN(${day_diff}) >= 7 and SUM(${cost}) != 0 THEN round(((SUM( ${lag_revenue_sum_day_7} ) - SUM( ${lag_af_purchase_sum_day_7} )*0.3) / SUM(${lag_cost})) * 100, 2)   ELSE null END
     ;;
    html: <p style="color: black; font-size: 18px">{{d0_roas}}% </p> ;;
  }
#round(((SUM( ${lag_revenue_sum_day_0} ) - SUM( ${lag_af_purchase_sum_day_0} )*0.3) / SUM(${lag_cost})) * 100, 2)
  measure: d0roas_total_change {
    type:  number
    value_format: "0.00\%"
    sql:  (${d0_roas}-${lag_d0_roas})*100/${lag_d0_roas} ;;
    #html: <p style="color: black; font-size: 18px">{{install_total}} </p> ;;
  }

  measure: d30_exp_roas_ios {
    type:  number
    value_format: "0.00%"
    sql:  round((SUM(CASE WHEN  ${platform} = 'IOS' and ${day_diff} <= 0 THEN ${d0_d30_exp_ltv}
    WHEN  ${platform} = 'IOS' and ${day_diff} <3 THEN ${d1_d30_exp_ltv}
    WHEN  ${platform} = 'IOS' and ${day_diff} < 7 THEN ${d3_d30_exp_ltv}
    WHEN  ${platform} = 'IOS' and ${day_diff} >= 7 THEN ${d7_d30_exp_ltv} END)  / SUM(CASE WHEN  ${platform} = 'IOS' THEN ${cost} END))*100, 2) ;;
    html: <p style="color: black; font-size: 18px">{{d30_exp_roas_ios}}% </p> ;;
  }

  measure: d30_exp_roas_android {
    type:  number
    value_format: "0.00%"
    sql:  round((SUM(CASE WHEN  ${platform} = 'Android' and ${day_diff} <= 0 THEN ${d0_d30_exp_ltv}
    WHEN  ${platform} = 'Android' and ${day_diff} <3 THEN ${d1_d30_exp_ltv}
    WHEN  ${platform} = 'Android' and ${day_diff} < 7 THEN ${d3_d30_exp_ltv}
    WHEN  ${platform} = 'Android' and ${day_diff} >= 7 THEN ${d7_d30_exp_ltv} END)  / SUM(CASE WHEN  ${platform} = 'Android' THEN ${cost} END))*100, 2) ;;
    html: <p style="color: black; font-size: 18px">{{d30_exp_roas_android}}% </p> ;;
  }

  measure: d30_exp_roas {
    type:  number
    value_format: "0.00%"
    sql:  case when MIN(${day_diff}) <= 0 and SUM(${cost}) != 0 THEN ROUND((SUM( ${d0_d30_exp_ltv} ) / SUM(${cost}))*100,2)
    when MIN(${day_diff}) < 3 and SUM(${cost}) != 0 THEN ROUND((SUM( ${d1_d30_exp_ltv} ) / SUM(${cost}))*100,2)
    when MIN(${day_diff}) <7 and SUM(${cost}) != 0 THEN ROUND((SUM( ${d3_d30_exp_ltv} ) / SUM(${cost}))*100,2)
    when MIN(${day_diff}) >= 7 and SUM(${cost}) != 0 THEN ROUND((SUM( ${d7_d30_exp_ltv} ) / SUM(${cost}))*100,2)  ELSE null END;;
    html: <p style="color: black; font-size: 18px">{{d30_exp_roas}}% </p> ;;
  }

  measure: d60_exp_roas_ios {
    type:  number
    value_format: "0.00%"
    sql:  round((SUM(CASE WHEN  ${platform} = 'IOS' and ${day_diff} <= 0 THEN ${d0_d60_exp_ltv}
    WHEN  ${platform} = 'IOS' and ${day_diff} <3 THEN ${d1_d60_exp_ltv}
    WHEN  ${platform} = 'IOS' and ${day_diff} < 7 THEN ${d3_d60_exp_ltv}
    WHEN  ${platform} = 'IOS' and ${day_diff} >= 7 THEN ${d7_d60_exp_ltv} END)  / SUM(CASE WHEN  ${platform} = 'IOS' THEN ${cost} END))*100, 2) ;;
    html: <p style="color: black; font-size: 18px">{{d60_exp_roas_ios}}% </p> ;;
    }

  measure: d60_exp_roas_android {
    type:  number
    value_format: "0.00%"
    sql: sql:  round((SUM(CASE WHEN  ${platform} = 'Android' and ${day_diff} <= 0 THEN ${d0_d60_exp_ltv}
    WHEN  ${platform} = 'Android' and ${day_diff} <3 THEN ${d1_d60_exp_ltv}
    WHEN  ${platform} = 'Android' and ${day_diff} < 7 THEN ${d3_d60_exp_ltv}
    WHEN  ${platform} = 'Android' and ${day_diff} >= 7 THEN ${d7_d60_exp_ltv} END)  / SUM(CASE WHEN  ${platform} = 'Android' THEN ${cost} END))*100, 2) ;;
    html: <p style="color: black; font-size: 18px">{{d60_exp_roas_android}}% </p> ;;
  }

  measure: d60_exp_roas {
    type:  number
    value_format: "0.00%"
    sql:  case when MIN(${day_diff}) <= 0 and SUM(${cost}) != 0 THEN ROUND((SUM( ${d0_d60_exp_ltv} ) / SUM(${cost}))*100,2)
    when MIN(${day_diff}) < 3 and SUM(${cost}) != 0 THEN ROUND((SUM( ${d1_d60_exp_ltv} ) / SUM(${cost}))*100,2)
    when MIN(${day_diff}) <7 and SUM(${cost}) != 0 THEN ROUND((SUM( ${d3_d60_exp_ltv} ) / SUM(${cost}))*100,2)
    when MIN(${day_diff}) >= 7 and SUM(${cost}) != 0 THEN ROUND((SUM( ${d7_d60_exp_ltv} ) / SUM(${cost}))*100,2)  ELSE null END;;
    html: <p style="color: black; font-size: 18px">{{d60_exp_roas}}% </p> ;;
  }

  measure: d90_exp_roas_ios {
    type:  number
    value_format: "0.00%"
    sql:  round((SUM(CASE WHEN  ${platform} = 'IOS' and ${day_diff} <= 0 THEN ${d0_d90_exp_ltv}
    WHEN  ${platform} = 'IOS' and ${day_diff} <3 THEN ${d1_d90_exp_ltv}
    WHEN  ${platform} = 'IOS' and ${day_diff} < 7 THEN ${d3_d90_exp_ltv}
    WHEN  ${platform} = 'IOS' and ${day_diff} >= 7 THEN ${d7_d90_exp_ltv} END)  / SUM(CASE WHEN  ${platform} = 'IOS' THEN ${cost} END))*100, 2) ;;
    html: <p style="color: black; font-size: 18px">{{d90_exp_roas_ios}}% </p> ;;
  }

  measure: d90_exp_roas_android {
    type:  number
    value_format: "0.00%"
    sql:  round((SUM(CASE WHEN  ${platform} = 'Android' and ${day_diff} <= 0 THEN ${d0_d90_exp_ltv}
    WHEN  ${platform} = 'Android' and ${day_diff} <3 THEN ${d1_d90_exp_ltv}
    WHEN  ${platform} = 'Android' and ${day_diff} < 7 THEN ${d3_d90_exp_ltv}
    WHEN  ${platform} = 'Android' and ${day_diff} >= 7 THEN ${d7_d90_exp_ltv} END)  / SUM(CASE WHEN  ${platform} = 'Android' THEN ${cost} END))*100, 2) ;;
    html: <p style="color: black; font-size: 18px">{{d90_exp_roas_android}}% </p> ;;
  }

  measure: d90_exp_roas {
    type:  number
    value_format: "0.00%"
    sql: case when MIN(${day_diff}) <= 0 and SUM(${cost}) != 0 THEN ROUND((SUM( ${d0_d90_exp_ltv} ) / SUM(${cost}))*100,2)
    when MIN(${day_diff}) < 3 and SUM(${cost}) != 0 THEN ROUND((SUM( ${d1_d90_exp_ltv} ) / SUM(${cost}))*100,2)
    when MIN(${day_diff}) <7 and SUM(${cost}) != 0 THEN ROUND((SUM( ${d3_d90_exp_ltv} ) / SUM(${cost}))*100,2)
    when MIN(${day_diff}) >= 7 and SUM(${cost}) != 0 THEN ROUND((SUM( ${d7_d90_exp_ltv} ) / SUM(${cost}))*100,2)  ELSE null END;;
    html: <p style="color: black; font-size: 18px">{{d90_exp_roas}}% </p> ;;
  }

  measure: d120_exp_roas_ios {
    type:  number
    value_format: "0.00%"
    sql:  round((SUM(CASE WHEN  ${platform} = 'IOS' and ${day_diff} <= 0 THEN ${d0_d120_exp_ltv}
    WHEN  ${platform} = 'IOS' and ${day_diff} <3 THEN ${d1_d120_exp_ltv}
    WHEN  ${platform} = 'IOS' and ${day_diff} < 7 THEN ${d3_d120_exp_ltv}
    WHEN  ${platform} = 'IOS' and ${day_diff} >= 7 THEN ${d7_d120_exp_ltv} END)  / SUM(CASE WHEN  ${platform} = 'IOS' THEN ${cost} END))*100, 2) ;;
    html: <p style="color: black; font-size: 18px">{{d120_exp_roas_ios}}% </p> ;;
  }

  measure: d120_exp_roas_android {
    type:  number
    value_format: "0.00%"
    sql:  round((SUM(CASE WHEN  ${platform} = 'Android' and ${day_diff} <= 0 THEN ${d0_d120_exp_ltv}
    WHEN  ${platform} = 'Android' and ${day_diff} <3 THEN ${d1_d120_exp_ltv}
    WHEN  ${platform} = 'Android' and ${day_diff} < 7 THEN ${d3_d120_exp_ltv}
    WHEN  ${platform} = 'Android' and ${day_diff} >= 7 THEN ${d7_d120_exp_ltv} END)  / SUM(CASE WHEN  ${platform} = 'Android' THEN ${cost} END))*100, 2) ;;
    html: <p style="color: black; font-size: 18px">{{d120_exp_roas_android}}% </p> ;;
  }

  measure: d120_exp_roas {
    type:  number
    value_format: "0.00%"
    sql:  case when MIN(${day_diff}) <= 0 and SUM(${cost}) != 0 THEN ROUND((SUM( ${d0_d120_exp_ltv} ) / SUM(${cost}))*100,2)
    when MIN(${day_diff}) < 3 and SUM(${cost}) != 0 THEN ROUND((SUM( ${d1_d120_exp_ltv} ) / SUM(${cost}))*100,2)
    when MIN(${day_diff}) <7 and SUM(${cost}) != 0 THEN ROUND((SUM( ${d3_d120_exp_ltv} ) / SUM(${cost}))*100,2)
    when MIN(${day_diff}) >= 7 and SUM(${cost}) != 0 THEN ROUND((SUM( ${d7_d120_exp_ltv} ) / SUM(${cost}))*100,2)  ELSE null END;;
    html: <p style="color: black; font-size: 18px">{{d120_exp_roas}}% </p> ;;
  }

  measure: d180_exp_roas_ios {
    type:  number
    value_format: "0.00%"
    sql: round((SUM(CASE WHEN  ${platform} = 'IOS' and ${day_diff} <= 0 THEN ${d0_d180_exp_ltv}
    WHEN  ${platform} = 'IOS' and ${day_diff} <3 THEN ${d1_d180_exp_ltv}
    WHEN  ${platform} = 'IOS' and ${day_diff} < 7 THEN ${d3_d180_exp_ltv}
    WHEN  ${platform} = 'IOS' and ${day_diff} >= 7 THEN ${d7_d180_exp_ltv} END)  / SUM(CASE WHEN  ${platform} = 'IOS' THEN ${cost} END))*100, 2) ;;
    html: <p style="color: black; font-size: 18px">{{d180_exp_roas_ios}}% </p> ;;
  }

  measure: d180_exp_roas_android {
    type:  number
    value_format: "0.00%"
    sql:  round((SUM(CASE WHEN  ${platform} = 'Android' and ${day_diff} <= 0 THEN ${d0_d180_exp_ltv}
    WHEN  ${platform} = 'Android' and ${day_diff} <3 THEN ${d1_d180_exp_ltv}
    WHEN  ${platform} = 'Android' and ${day_diff} < 7 THEN ${d3_d180_exp_ltv}
    WHEN  ${platform} = 'Android' and ${day_diff} >= 7 THEN ${d7_d180_exp_ltv} END)  / SUM(CASE WHEN  ${platform} = 'Android' THEN ${cost} END))*100, 2) ;;
    html: <p style="color: black; font-size: 18px">{{d180_exp_roas_android}}% </p> ;;
  }

  measure: d180_exp_roas {
    type:  number
    value_format: "0.00%"
    sql:  case when MIN(${day_diff}) <= 0 THEN round((SUM( ${d0_d180_exp_ltv} ) / SUM(${cost})) * 100 , 2)
    when MIN(${day_diff}) < 3 THEN round((SUM( ${d1_d180_exp_ltv} ) / SUM(${cost})) * 100 , 2)
    when MIN(${day_diff}) <7 THEN round((SUM( ${d3_d180_exp_ltv} ) / SUM(${cost})) * 100 , 2)
    ELSE round((SUM( ${d7_d180_exp_ltv} ) / SUM(${cost})) * 100 , 2) END ;;
    html: <p style="color: black; font-size: 18px">{{d180_exp_roas}}% </p> ;;
  }

  measure: d270_exp_roas_ios {
    type:  number
    value_format: "0.00%"
    sql: round((SUM(CASE WHEN  ${platform} = 'IOS' and ${day_diff} <= 0 THEN ${d0_d270_exp_ltv}
          WHEN  ${platform} = 'IOS' and ${day_diff} <3 THEN ${d1_d270_exp_ltv}
          WHEN  ${platform} = 'IOS' and ${day_diff} < 7 THEN ${d3_d270_exp_ltv}
          WHEN  ${platform} = 'IOS' and ${day_diff} >= 7 THEN ${d7_d270_exp_ltv} END)  / SUM(CASE WHEN  ${platform} = 'IOS' THEN ${cost} END))*100, 2) ;;
    html: <p style="color: black; font-size: 18px">{{d270_exp_roas_ios}}% </p> ;;
  }

  measure: d270_exp_roas_android {
    type:  number
    value_format: "0.00%"
    sql:  round((SUM(CASE WHEN  ${platform} = 'Android' and ${day_diff} <= 0 THEN ${d0_d270_exp_ltv}
          WHEN  ${platform} = 'Android' and ${day_diff} <3 THEN ${d1_d270_exp_ltv}
          WHEN  ${platform} = 'Android' and ${day_diff} < 7 THEN ${d3_d270_exp_ltv}
          WHEN  ${platform} = 'Android' and ${day_diff} >= 7 THEN ${d7_d270_exp_ltv} END)  / SUM(CASE WHEN  ${platform} = 'Android' THEN ${cost} END))*100, 2) ;;
    html: <p style="color: black; font-size: 18px">{{d270_exp_roas_android}}% </p> ;;
  }

  measure: d270_exp_roas {
    type:  number
    value_format: "0.00%"
    sql:  case when MIN(${day_diff}) <= 0 THEN round((SUM( ${d0_d270_exp_ltv} ) / SUM(${cost})) * 100 , 2)
          when MIN(${day_diff}) < 3 THEN round((SUM( ${d1_d270_exp_ltv} ) / SUM(${cost})) * 100 , 2)
          when MIN(${day_diff}) <7 THEN round((SUM( ${d3_d270_exp_ltv} ) / SUM(${cost})) * 100 , 2)
          ELSE round((SUM( ${d7_d270_exp_ltv} ) / SUM(${cost})) * 100 , 2) END ;;
    html: <p style="color: black; font-size: 18px">{{d270_exp_roas}}% </p> ;;
  }

  measure: d360_exp_roas_ios {
    type:  number
    value_format: "0.00%"
    sql: round((SUM(CASE WHEN  ${platform} = 'IOS' and ${day_diff} <= 0 THEN ${d0_d360_exp_ltv}
          WHEN  ${platform} = 'IOS' and ${day_diff} <3 THEN ${d1_d360_exp_ltv}
          WHEN  ${platform} = 'IOS' and ${day_diff} < 7 THEN ${d3_d360_exp_ltv}
          WHEN  ${platform} = 'IOS' and ${day_diff} >= 7 THEN ${d7_d360_exp_ltv} END)  / SUM(CASE WHEN  ${platform} = 'IOS' THEN ${cost} END))*100, 2) ;;
    html: <p style="color: black; font-size: 18px">{{d360_exp_roas_ios}}% </p> ;;
  }

  measure: d360_exp_roas_android {
    type:  number
    value_format: "0.00%"
    sql:  round((SUM(CASE WHEN  ${platform} = 'Android' and ${day_diff} <= 0 THEN ${d0_d360_exp_ltv}
          WHEN  ${platform} = 'Android' and ${day_diff} <3 THEN ${d1_d360_exp_ltv}
          WHEN  ${platform} = 'Android' and ${day_diff} < 7 THEN ${d3_d360_exp_ltv}
          WHEN  ${platform} = 'Android' and ${day_diff} >= 7 THEN ${d7_d360_exp_ltv} END)  / SUM(CASE WHEN  ${platform} = 'Android' THEN ${cost} END))*100, 2) ;;
    html: <p style="color: black; font-size: 18px">{{d360_exp_roas_android}}% </p> ;;
  }

  measure: d360_exp_roas {
    type:  number
    value_format: "0.00%"
    sql:  case when MIN(${day_diff}) <= 0 THEN round((SUM( ${d0_d360_exp_ltv} ) / SUM(${cost})) * 100 , 2)
          when MIN(${day_diff}) < 3 THEN round((SUM( ${d1_d360_exp_ltv} ) / SUM(${cost})) * 100 , 2)
          when MIN(${day_diff}) <7 THEN round((SUM( ${d3_d360_exp_ltv} ) / SUM(${cost})) * 100 , 2)
          ELSE round((SUM( ${d7_d360_exp_ltv} ) / SUM(${cost})) * 100 , 2) END ;;
    html: <p style="color: black; font-size: 18px">{{d360_exp_roas}}% </p> ;;
  }

  measure: d720_exp_roas_ios {
    type:  number
    value_format: "0.00%"
    sql: round((SUM(CASE WHEN  ${platform} = 'IOS' and ${day_diff} <= 0 THEN ${d0_d720_exp_ltv}
          WHEN  ${platform} = 'IOS' and ${day_diff} <3 THEN ${d1_d720_exp_ltv}
          WHEN  ${platform} = 'IOS' and ${day_diff} < 7 THEN ${d3_d720_exp_ltv}
          WHEN  ${platform} = 'IOS' and ${day_diff} >= 7 THEN ${d7_d720_exp_ltv} END)  / SUM(CASE WHEN  ${platform} = 'IOS' THEN ${cost} END))*100, 2) ;;
    html: <p style="color: black; font-size: 18px">{{d720_exp_roas_ios}}% </p> ;;
  }

  measure: d720_exp_roas_android {
    type:  number
    value_format: "0.00%"
    sql:  round((SUM(CASE WHEN  ${platform} = 'Android' and ${day_diff} <= 0 THEN ${d0_d720_exp_ltv}
          WHEN  ${platform} = 'Android' and ${day_diff} <3 THEN ${d1_d720_exp_ltv}
          WHEN  ${platform} = 'Android' and ${day_diff} < 7 THEN ${d3_d720_exp_ltv}
          WHEN  ${platform} = 'Android' and ${day_diff} >= 7 THEN ${d7_d720_exp_ltv} END)  / SUM(CASE WHEN  ${platform} = 'Android' THEN ${cost} END))*100, 2) ;;
    html: <p style="color: black; font-size: 18px">{{d720_exp_roas_android}}% </p> ;;
  }

  measure: d720_exp_roas {
    type:  number
    value_format: "0.00%"
    sql:  case when MIN(${day_diff}) <= 0 THEN round((SUM( ${d0_d720_exp_ltv} ) / SUM(${cost})) * 100 , 2)
          when MIN(${day_diff}) < 3 THEN round((SUM( ${d1_d720_exp_ltv} ) / SUM(${cost})) * 100 , 2)
          when MIN(${day_diff}) <7 THEN round((SUM( ${d3_d720_exp_ltv} ) / SUM(${cost})) * 100 , 2)
          ELSE round((SUM( ${d7_d720_exp_ltv} ) / SUM(${cost})) * 100 , 2) END ;;
    html: <p style="color: black; font-size: 18px">{{d720_exp_roas}}% </p> ;;
  }

  measure: d14_exp_roas_ios {
    type:  number
    value_format: "0.00%"
    sql: round((SUM(CASE WHEN  ${platform} = 'IOS' and ${day_diff} <= 0 THEN ${d0_d14_exp_ltv}
          WHEN  ${platform} = 'IOS' and ${day_diff} <3 THEN ${d1_d14_exp_ltv}
          WHEN  ${platform} = 'IOS' and ${day_diff} < 7 THEN ${d3_d14_exp_ltv}
          WHEN  ${platform} = 'IOS' and ${day_diff} >= 7 THEN ${d7_d14_exp_ltv} END)  / SUM(CASE WHEN  ${platform} = 'IOS' THEN ${cost} END))*100, 2) ;;
    html: <p style="color: black; font-size: 18px">{{d14_exp_roas_ios}}% </p> ;;
  }

  measure: d14_exp_roas_android {
    type:  number
    value_format: "0.00%"
    sql:  round((SUM(CASE WHEN  ${platform} = 'Android' and ${day_diff} <= 0 THEN ${d0_d14_exp_ltv}
          WHEN  ${platform} = 'Android' and ${day_diff} <3 THEN ${d1_d14_exp_ltv}
          WHEN  ${platform} = 'Android' and ${day_diff} < 7 THEN ${d3_d14_exp_ltv}
          WHEN  ${platform} = 'Android' and ${day_diff} >= 7 THEN ${d7_d14_exp_ltv} END)  / SUM(CASE WHEN  ${platform} = 'Android' THEN ${cost} END))*100, 2) ;;
    html: <p style="color: black; font-size: 18px">{{d14_exp_roas_android}}% </p> ;;
  }

  measure: d14_exp_roas {
    type:  number
    value_format: "0.00%"
    sql:  case when MIN(${day_diff}) <= 0 THEN round((SUM( ${d0_d14_exp_ltv} ) / SUM(${cost})) * 100 , 2)
          when MIN(${day_diff}) < 3 THEN round((SUM( ${d1_d14_exp_ltv} ) / SUM(${cost})) * 100 , 2)
          when MIN(${day_diff}) <7 THEN round((SUM( ${d3_d14_exp_ltv} ) / SUM(${cost})) * 100 , 2)
          ELSE round((SUM( ${d7_d14_exp_ltv} ) / SUM(${cost})) * 100 , 2) END ;;
    html: <p style="color: black; font-size: 18px">{{d14_exp_roas}}% </p> ;;
  }

  measure: count {
    type: count
    drill_fields: [app_name]
  }

  dimension: total_tile {
    type: string
    sql: ${TABLE}.total ;;
    html: <p style="color: black; font-size: 18px">{{total_tile}} </p> ;;
  }

  dimension: total_tile2 {
    type: string
    sql: ${TABLE}.total ;;
    html: <p style="color: black; font-size: 18px">{{total_tile2}} </p> ;;
    #html: <div style="height: 60px; border: 3px solid #63666A; border-radius: 15px; font-family: Google Sans,Noto Sans,Noto Sans JP,Noto Sans KR,Noto Naskh Arabic,Noto Sans Thai,Noto Sans Hebrew,Noto Sans Bengali,sans-serif; padding: 8px; display: flex;">
#<h2 style="width:100%;height:90%;text-align:center;color: #1A73E8; line-height: 1.2rem; font-size: 15px">{{total_tile2}}</h2>
#<div>;;
  }

}
