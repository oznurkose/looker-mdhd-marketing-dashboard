view: campaign_aggregated_prediction {
  derived_table: {
    sql: SELECT *,concat("D",day_diff) as day_diff_str FROM `mergedom-home-design.api_connection.campaign_aggregate_roas_prediction`
      ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  dimension: date {
    type: date
    sql: cast(date(${TABLE}.date) as timestamp) ;;
  }
  dimension: day_diff_str {
    type: string
    sql: ${TABLE}.day_diff_str ;;
  }

  dimension_group: run_time {
    type: time
    datatype: datetime
    sql: ${TABLE}.run_time ;;
  }

  dimension: day_diff {
    type: number
    sql: ${TABLE}.day_diff ;;
  }

  dimension: agg_30_adj {
    type: number
    sql: ${TABLE}.agg_30_adj ;;
  }

  dimension: agg_60_adj {
    type: number
    sql: ${TABLE}.agg_60_adj ;;
  }

  dimension: agg_90_adj {
    type: number
    sql: ${TABLE}.agg_90_adj ;;
  }

  dimension: agg_120_adj {
    type: number
    sql: ${TABLE}.agg_120_adj ;;
  }

  dimension: agg_180_adj {
    type: number
    sql: ${TABLE}.agg_180_adj ;;
  }

  dimension: agg_270_adj {
    type: number
    sql: ${TABLE}.agg_270_adj ;;
  }

  dimension: agg_360_adj {
    type: number
    sql: ${TABLE}.agg_360_adj ;;
  }

  dimension: agg_720_adj {
    type: number
    sql: ${TABLE}.agg_720_adj ;;
  }

  dimension: platform {
    type: string
    sql: ${TABLE}.platform ;;
  }

  dimension: cost {
    type: number
    sql: ${TABLE}.cost ;;
  }

  dimension: revenue {
    type: number
    sql: ${TABLE}.Revenue ;;
  }

  measure: total_30 {
    type: number
    sql: sum(${pred_30}*${cost})/sum(${cost}) ;;
    #html: <p style="color: black; font-size: 10px">{{total_30}} </p> ;;
  }

  dimension: pred_30 {
    type: number
    sql: ${TABLE}.pred_30 ;;
  }

  measure: pred_30_m {
    type:  number
    value_format: "0.00%"
    sql:  sum(${pred_30});;
    #html: <p style="color: black; font-size: 30px">{{pred_30_m}}% </p> ;;

  }

  measure: total_60 {
    type: number
    sql: sum(${pred_60}*${cost})/sum(${cost}) ;;
    #html: <p style="color: black; font-size: 14px">{{total_60}} </p> ;;

  }


  dimension: pred_60 {
    type: number
    sql: ${TABLE}.pred_60 ;;
  }

  measure: pred_60_m {
    type:  number
    value_format: "0.00%"
    sql:  sum(${pred_60});;
    #html: <p style="color: black; font-size: 14px">{{pred_60_m}} </p> ;;

  }

  measure: total_90 {
    type: number
    sql: sum(${pred_90}*${cost})/sum(${cost}) ;;
    #html: <p style="color: black; font-size: 14px">{{total_90}} </p> ;;
  }

  dimension: pred_90 {
    type: number
    sql: ${TABLE}.pred_90 ;;
  }

  measure: pred_90_m {
    type:  number
    value_format: "0.00%"
    sql:  sum(${pred_90});;
    #html: <p style="color: black; font-size: 14px">{{pred_90_m}} </p> ;;

  }

  measure: total_120 {
    type: number
    sql: sum(${pred_120}*${cost})/sum(${cost}) ;;
    #html: <p style="color: black; font-size: 14px">{{total_120}} </p> ;;
  }

  dimension: pred_120 {
    type: number
    sql: ${TABLE}.pred_120 ;;
  }

  measure: pred_120_m {
    type:  number
    value_format: "0.00%"
    sql:  sum(${pred_120});;
    #html: <p style="color: black; font-size: 14px">{{pred_120_m}} </p> ;;

  }

  measure: total_180 {
    type: number
    sql: sum(${pred_180}*${cost})/sum(${cost}) ;;
    #html: <p style="color: black; font-size: 14px">{{total_180}} </p> ;;
  }

  dimension: pred_180 {
    type: number
    sql: ${TABLE}.pred_180 ;;
  }

  measure: pred_180_m {
    type:  number
    value_format: "0.00%"
    sql:  sum(${pred_180});;
    #html: <p style="color: black; font-size: 14px">{{pred_180_m}} </p> ;;

  }

  dimension: pred_270 {
    type: number
    sql: ${TABLE}.pred_270 ;;
  }

  dimension: pred_360 {
    type: number
    sql: ${TABLE}.pred_360 ;;
  }

  dimension: pred_720 {
    type: number
    sql: ${TABLE}.pred_720 ;;
  }

  set: detail {
    fields: [
      date,
      run_time_time,
      day_diff,
      agg_30_adj,
      agg_60_adj,
      agg_90_adj,
      agg_120_adj,
      agg_180_adj,
      agg_270_adj,
      agg_360_adj,
      agg_720_adj,
      platform,
      cost,
      revenue,
      pred_30,
      pred_60,
      pred_90,
      pred_120,
      pred_180,
      pred_270,
      pred_360,
      pred_720
    ]
  }
}
