view: platform_tier_ltv_forecast_lag {
   # Or, you could make this view a derived table, like this:
   derived_table: {
     sql: With Temp1 as (
    SELECT install_time,platform,tier,date_diff( date(run_time), date(install_time)+2, day) as day_diff,installs,run_time, cost, case when installs = 0 THEN Null ELSE cost/installs END as cpi,total_revenue-iap_revenue*0.3 as total_revenue,dau,iap_revenue, case when dau = 0 THEN NULL ELSE (total_revenue-iap_revenue*0.3)/dau END as ARPDAU, Case when cost=0 then null
    Else (revenue_sum_day_0-af_purchase_sum_day_0*0.3)/cost END as d0Roas,
    LEAD(installs) over (PARTITION BY platform,tier,date_diff( date(run_time), date(install_time)+2, day) ORDER BY install_time desc) as lag_install,
    LEAD(cost) over (PARTITION BY platform,tier,date_diff( date(run_time), date(install_time)+2, day) ORDER BY install_time desc) as lag_cost
    FROM `mergedom-home-design.api_connection.platform_tier_ltv_forecast`)

    select *,concat("D",date_diff( date(run_time), date(install_time)+2, day)) as day_diff_str,
    LEAD(d0Roas) over (PARTITION BY platform,tier,day_diff ORDER BY install_time desc) as lag_d0Roas,
    LEAD(ARPDAU) over (PARTITION BY platform,tier,day_diff ORDER BY install_time desc) as lag_ARPDAU,
    LEAD(total_revenue) over (PARTITION BY platform,tier,day_diff ORDER BY install_time desc) as lag_total_revenue,
    LEAD(cpi) over (PARTITION BY platform,tier,day_diff ORDER BY install_time desc) as lag_cpi from Temp1


       ;;


   }

   # Define your dimensions and measures here, like this:

    dimension: cost {
      type: number
      sql: ${TABLE}.cost ;;
    }

    dimension: day_diff {
      type: number
      sql: ${TABLE}.day_diff ;;
    }


    dimension: cpi {
      type: number
      sql: ${TABLE}.cpi ;;
    }

    dimension: lag_cpi {
      type: number
      sql: ${TABLE}.lag_cpi ;;
    }


    dimension: dau {
      type: number
      sql: ${TABLE}.dau ;;
    }

    dimension: install_time {
      type: string
      sql: ${TABLE}.install_time ;;
    }

    dimension: install_date {
      type: date_time
      sql: cast(date(${TABLE}.install_time) as timestamp);;
    }


    dimension: installs {
      type: number
      sql: ${TABLE}.installs ;;
    }


    dimension: ARPDAU {
      type: number
      sql: ${TABLE}.ARPDAU ;;
    }

    dimension: lag_ARPDAU {
      type: number
      sql: ${TABLE}.lag_ARPDAU ;;
    }

    dimension: lag_install {
      type: number
      sql: ${TABLE}.lag_install ;;
    }

    dimension: lag_cost {
      type: number
      sql: ${TABLE}.lag_cost ;;
    }

    dimension: lag_total_revenue {
      type: number
      sql: ${TABLE}.lag_total_revenue ;;
    }

    dimension: platform {
      type: string
      sql: case when ${TABLE}.platform = "android" THEN "Android" Else "IOS" END ;;
    }

    dimension_group: run {
      type: time
      timeframes: [
        raw,
        time,
        date,
        week,
        month,
        quarter,
        year
      ]
      sql: ${TABLE}.run_time ;;
    }

    dimension: tier {
      type: string
      sql: ${TABLE}.tier ;;
    }

    dimension: tier_ordered {
      type: number
      sql: case when ${TABLE}.tier = "US" THEN 0
      when  ${TABLE}.tier = "Tier1" THEN 1
      when  ${TABLE}.tier = "Tier2" THEN 2
      when  ${TABLE}.tier = "Tier3" THEN 3
      when  ${TABLE}.tier = "Tier4" THEN 4
      when  ${TABLE}.tier = "Tier5" THEN 5
      when  ${TABLE}.tier = "ROW" THEN 6 END;;
    }


    dimension: total_revenue {
      type: number
      sql: ${TABLE}.total_revenue
      ;;
      value_format: "#,##0"
    }
    dimension: d0Roas {
      type: number
      sql: ${TABLE}.d0Roas ;;
      html:
      {% if d0Roas_change._value >= 0 %}

      <div style="background-color: rgba(0,210,15,{{d0Roas_change_scaled._value}}); text-align:center"><a  style="color: black" target="_new">{{rendered_value}} </a></div>
      {% else %}
      <div style="background-color: rgba(255,0,0,{{d0Roas_change_scaled._value}}); text-align:center"><a  style="color: black" target="_new">{{rendered_value }} </a></div>

      {% endif %}

      ;;
    }
    dimension: lag_d0Roas {
      type: number
      sql: ${TABLE}.lag_d0Roas ;;
    }

    measure: install_change {
      type: number
      value_format: "0.00\%"
      sql:  case when SUM(${lag_install}) = 0 THEN Null ELSE (SUM(${installs}) - SUM( ${lag_install}))*100 / SUM(${lag_install}) END ;;
    }



    measure: install_change_scaled {
      type:  number
      value_format: "0.00\%"
      sql:  case when ${install_change} >= 0 THEN ${install_change}/100
        ELSE abs(${install_change})/100 END ;;
    }

    dimension: day_diff_str {
      type:  string
      sql: ${TABLE}.day_diff_str;;
    }


    measure: total_installs {
      type: number
      value_format: "#,##0"
      sql:  SUM(${installs}) ;;

    }

    measure: total_cost {
      type: number
      value_format: "#,##0"
      sql:  SUM(${cost}) ;;

    }

    measure: cost_change {
      type:  number
      value_format: "0.00\%"
      sql:  case when SUM(${lag_cost}) = 0 THEN NULL ELSE (SUM(${cost}) - SUM( ${lag_cost}))*100 / SUM(${lag_cost}) END  ;;

    }

    measure: cost_change_scaled {
      type:  number
      value_format: "0.00\%"
      sql:  case when ${cost_change} >= 0 THEN ${cost_change}/100
        ELSE abs(${cost_change})/100 END ;;
    }

    measure: totalrev_change {
      type:  number
      value_format: "0.00\%"
      sql:  Case when  SUM(${lag_total_revenue}) = 0 THEN null ELSE (SUM(${total_revenue}) - SUM( ${lag_total_revenue}))*100 / SUM(${lag_total_revenue}) END ;;

    }

    measure: totalrev_change_scaled {
      type:  number
      value_format: "0.00\%"
      sql:  case when ${totalrev_change} >= 0 THEN ${totalrev_change}/100
        ELSE abs(${totalrev_change})/100 END ;;
    }

    measure: ARPDAU_change {
      type:  number
      value_format: "0.00\%"
      sql:  case when SUM(${lag_ARPDAU}) = 0 THEN NULL ELSE (SUM(${ARPDAU}) - SUM( ${lag_ARPDAU}))*100 / SUM(${lag_ARPDAU}) END ;;

    }

    measure: ARPDAU_scaled {
      type:  number
      value_format: "0.00\%"
      sql:  case when ${ARPDAU_change} >= 0 THEN ${ARPDAU_change}/100
        ELSE abs(${ARPDAU_change})/100 END ;;
    }


    measure: d0Roas_change_scaled {
      type:  number
      value_format: "0.00\%"
      sql:  case when ${d0Roas_change} >= 0 THEN ${d0Roas_change}/100
      ELSE abs(${d0Roas_change})/100 END ;;
    }

    measure: d0Roas_change {
      type:  number
      value_format: "0.00\%"
      sql:  case when SUM(${lag_d0Roas}) = 0 THEN NULL ELSE (SUM(${d0Roas}) - SUM( ${lag_d0Roas}))*100 / SUM(${lag_d0Roas}) END ;;
      html:
      {% if value >= 0 %}

      <div style="background-color: rgba(0,210,15,{{d0Roas_change_scaled._value}}); text-align:center"><a  style="color: green" target="_new">{{rendered_value}} </a></div>
      {% else %}
      <div style="background-color: rgba(255,0,0,{{d0Roas_change_scaled._value}}); text-align:center"><a  style="color: red" target="_new">{{rendered_value }} </a></div>

      {% endif %}

      ;;
    }

    measure: CPI_change_scaled {
      type:  number
      value_format: "0.00\%"
      sql:  case when ${CPI_change} >= 0 THEN ${CPI_change}/100
        ELSE abs(${CPI_change})/100 END ;;
    }

    measure: CPI_change {
      type:  number
      value_format: "0.00\%"
      sql:  case when SUM(${lag_cpi}) = 0 THEN NULL ELSE (SUM(${cpi}) - SUM( ${lag_cpi}))*100 / SUM(${lag_cpi}) END ;;
      html:
      {% if value >= 0 %}

      <div style="background-color: rgba(0,210,15,{{CPI_change_scaled._value}}); text-align:center"><a  style="color: black" target="_new">{{rendered_value}} </a></div>
      {% else %}
      <div style="background-color: rgba(255,0,0,{{CPI_change_scaled._value}}); text-align:center"><a  style="color: black" target="_new">{{rendered_value }} </a></div>

      {% endif %}

      ;;
    }



          }
