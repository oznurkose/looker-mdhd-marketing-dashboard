connection: "bigquery-analytics"

# include all the views
include: "/views/**/*.view"


datagroup: platform_ltv_forecast_default_datagroup {
   #sql_trigger: SELECT MAX(run_time) FROM `mergedom-home-design.api_connection.platform_ltv_forecast`;;
  sql_trigger: SELECT EXTRACT(day from TIMESTAMP_ADD(CURRENT_TIMESTAMP(), INTERVAL 14 HOUR));;
   max_cache_age: "24 hours"
}

datagroup: platform_ltv_tier_forecast_default_datagroup {
  #sql_trigger: SELECT MAX(run_time) FROM `mergedom-home-design.api_connection.platform_tier_ltv_forecast`;;
  sql_trigger: SELECT EXTRACT(day from TIMESTAMP_ADD(CURRENT_TIMESTAMP(), INTERVAL 14 HOUR));;
  max_cache_age: "24 hours"
}


datagroup: ad_revenue_default_datagroup {
  #sql_trigger: SELECT MAX(date) FROM `mergedom-home-design.analytics_263095698.daily_revenue`;;
  sql_trigger: SELECT EXTRACT(day from TIMESTAMP_ADD(CURRENT_TIMESTAMP(), INTERVAL 14 HOUR));;
  max_cache_age: "24 hours"
}

explore: accuracy_all_base_days {

  persist_with: platform_ltv_forecast_default_datagroup
}

#persist_with: platform_ltv_forecast_default_datagroup

explore: platform_ltv_forecast {
  persist_with: platform_ltv_forecast_default_datagroup

}

explore: campaign_aggregated_prediction {
  persist_with: platform_ltv_forecast_default_datagroup

  join: platform_ltv_forecast {
    type: left_outer
    relationship: one_to_one
    sql_on: ${campaign_aggregated_prediction.date} = ${platform_ltv_forecast.install_date} and
      ${campaign_aggregated_prediction.platform} = ${platform_ltv_forecast.platform} and ${campaign_aggregated_prediction.day_diff_str} = ${platform_ltv_forecast.day_diff_str};;
  }
}

explore: acc_check_ltv {
  persist_with: platform_ltv_forecast_default_datagroup

  join: campaign_aggregated_prediction {
    type: left_outer
    relationship: one_to_one
    sql_on: ${campaign_aggregated_prediction.date} = ${acc_check_ltv.install_time} and
      ${campaign_aggregated_prediction.platform} = ${acc_check_ltv.platform} and ${campaign_aggregated_prediction.day_diff} = ${acc_check_ltv.day_diff};;
  }

  join: linear_model_ios_180_prediction {
    type: left_outer
    relationship: one_to_one
    sql_on: ${linear_model_ios_180_prediction.date} = ${acc_check_ltv.install_time};;
  }

  join: linear_model_and_180_prediction {
    type: left_outer
    relationship: one_to_one
    sql_on: ${linear_model_and_180_prediction.date} = ${acc_check_ltv.install_time};;
  }
}


explore: ad_revenue_unpivoted {
  persist_with: ad_revenue_default_datagroup
}

explore: title_tiles {
  persist_with: platform_ltv_tier_forecast_default_datagroup
}



explore: platform_tier_ltv_forecast {
  persist_with: platform_ltv_tier_forecast_default_datagroup

  join: platform_ltv_forecast {
    type: left_outer
    relationship: many_to_one
    sql_on: ${platform_tier_ltv_forecast.install_date} = ${platform_ltv_forecast.install_date} and
          ${platform_tier_ltv_forecast.platform} = ${platform_ltv_forecast.platform} and ${platform_tier_ltv_forecast.day_diff_str} = ${platform_ltv_forecast.day_diff_str};;
  }

}


explore: platform_tier_ltv_forecast_lag {
  persist_with: platform_ltv_tier_forecast_default_datagroup
  join: platform_tier_ltv_forecast {
    type: inner
    relationship: one_to_one
    sql_on: ${platform_tier_ltv_forecast.install_date} = ${platform_tier_ltv_forecast_lag.install_date} and
    ${platform_tier_ltv_forecast.platform} = ${platform_tier_ltv_forecast_lag.platform} and
    ${platform_tier_ltv_forecast.tier} = ${platform_tier_ltv_forecast_lag.tier}
    and ${platform_tier_ltv_forecast.day_diff_str} = ${platform_tier_ltv_forecast_lag.day_diff_str};;
  }


  join: platform_ltv_forecast {
    type: left_outer
    relationship: many_to_one
    sql_on:  ${platform_ltv_forecast.install_date} = ${platform_tier_ltv_forecast_lag.install_date} and
    ${platform_ltv_forecast.platform} = ${platform_tier_ltv_forecast_lag.platform}
    and ${platform_ltv_forecast.day_diff_str} = ${platform_tier_ltv_forecast_lag.day_diff_str} ;;
  }



}





include: "/dashboards/**/*.dashboard"
