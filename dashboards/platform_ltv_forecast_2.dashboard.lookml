- dashboard: daily_marketing_report_3
  title: Daily Marketing Report 3
  layout: newspaper
  preferred_viewer: dashboards-next
  description: ''
  elements:
  - title: Installs Total
    name: Installs Total
    model: platform_ltv_forecast
    explore: platform_ltv_forecast
    type: single_value
    fields: [platform_ltv_forecast.install_total, platform_ltv_forecast.install_date]
    sorts: [platform_ltv_forecast.install_date desc]
    limit: 500
    dynamic_fields: [{category: table_calculation, label: Installs Total Change, value_format: !!null '',
        value_format_name: percent_2, calculation_type: percent_difference_from_previous,
        table_calculation: installs_total_change, args: [platform_ltv_forecast.install_total],
        _kind_hint: measure, _type_hint: number}]
    custom_color_enabled: true
    show_single_value_title: false
    show_comparison: true
    comparison_type: change
    comparison_reverse_colors: false
    show_comparison_label: false
    enable_conditional_formatting: true
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    custom_color: ''
    conditional_formatting: [{type: equal to, value: !!null '', background_color: "#1A73E8",
        font_color: !!null '', color_application: {collection_id: 7c56cc21-66e4-41c9-81ce-a60e1c3967b2,
          palette_id: 56d0c358-10a0-4fd6-aa0b-b117bef527ab}, bold: false, italic: false,
        strikethrough: false, fields: !!null ''}]
    series_types: {}
    defaults_version: 1
    listen: {}
    row: 2
    col: 2
    width: 2
    height: 2
  - title: Installs IOS
    name: Installs IOS
    model: platform_ltv_forecast
    explore: platform_ltv_forecast
    type: single_value
    fields: [platform_ltv_forecast.installs_ios]
    limit: 500
    custom_color_enabled: true
    show_single_value_title: false
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    series_types: {}
    defaults_version: 1
    listen: {}
    row: 4
    col: 2
    width: 2
    height: 2
  - title: Installs Android
    name: Installs Android
    model: platform_ltv_forecast
    explore: platform_ltv_forecast
    type: single_value
    fields: [platform_ltv_forecast.installs_android]
    limit: 500
    custom_color_enabled: true
    show_single_value_title: false
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    series_types: {}
    defaults_version: 1
    listen: {}
    row: 6
    col: 2
    width: 2
    height: 2
  - title: Cost Total
    name: Cost Total
    model: platform_ltv_forecast
    explore: platform_ltv_forecast
    type: single_value
    fields: [platform_ltv_forecast.cost_total, platform_ltv_forecast.install_time]
    sorts: [platform_ltv_forecast.install_time desc]
    limit: 500
    dynamic_fields: [{category: table_calculation, label: Cost Total Change, value_format: !!null '',
        value_format_name: percent_2, calculation_type: percent_difference_from_previous,
        table_calculation: cost_total_change, args: [platform_ltv_forecast.cost_total],
        _kind_hint: measure, _type_hint: number}]
    custom_color_enabled: true
    show_single_value_title: false
    show_comparison: true
    comparison_type: change
    comparison_reverse_colors: true
    show_comparison_label: false
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    series_types: {}
    defaults_version: 1
    listen: {}
    row: 2
    col: 4
    width: 2
    height: 2
  - title: Cost IOS
    name: Cost IOS
    model: platform_ltv_forecast
    explore: platform_ltv_forecast
    type: single_value
    fields: [platform_ltv_forecast.cost_ios]
    limit: 500
    custom_color_enabled: true
    show_single_value_title: false
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    series_types: {}
    defaults_version: 1
    listen: {}
    row: 4
    col: 4
    width: 2
    height: 2
  - title: Cost Android
    name: Cost Android
    model: platform_ltv_forecast
    explore: platform_ltv_forecast
    type: single_value
    fields: [platform_ltv_forecast.cost_android]
    limit: 500
    custom_color_enabled: true
    show_single_value_title: false
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    series_types: {}
    defaults_version: 1
    listen: {}
    row: 6
    col: 4
    width: 2
    height: 2
  - title: CPI Total
    name: CPI Total
    model: platform_ltv_forecast
    explore: platform_ltv_forecast
    type: single_value
    fields: [platform_ltv_forecast.cpi_total, platform_ltv_forecast.install_date]
    sorts: [platform_ltv_forecast.install_date desc]
    limit: 500
    dynamic_fields: [{category: table_calculation, label: CPI Total Change, value_format: !!null '',
        value_format_name: percent_2, calculation_type: percent_difference_from_previous,
        table_calculation: cpi_total_change, args: [platform_ltv_forecast.cpi_total],
        _kind_hint: measure, _type_hint: number}]
    custom_color_enabled: true
    show_single_value_title: false
    show_comparison: true
    comparison_type: change
    comparison_reverse_colors: true
    show_comparison_label: false
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    series_types: {}
    defaults_version: 1
    listen: {}
    row: 2
    col: 6
    width: 2
    height: 2
  - title: CPI IOS
    name: CPI IOS
    model: platform_ltv_forecast
    explore: platform_ltv_forecast
    type: single_value
    fields: [platform_ltv_forecast.cpi_ios]
    limit: 500
    custom_color_enabled: true
    show_single_value_title: false
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    series_types: {}
    defaults_version: 1
    listen: {}
    row: 4
    col: 6
    width: 2
    height: 2
  - title: CPI Android
    name: CPI Android
    model: platform_ltv_forecast
    explore: platform_ltv_forecast
    type: single_value
    fields: [platform_ltv_forecast.cpi_android]
    limit: 500
    custom_color_enabled: true
    show_single_value_title: false
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    series_types: {}
    defaults_version: 1
    listen: {}
    row: 6
    col: 6
    width: 2
    height: 2
  - title: Revenue Total
    name: Revenue Total
    model: platform_ltv_forecast
    explore: platform_ltv_forecast
    type: single_value
    fields: [platform_ltv_forecast.revenue_total, platform_ltv_forecast.install_time]
    sorts: [platform_ltv_forecast.install_time desc]
    limit: 500
    dynamic_fields: [{category: table_calculation, label: Revenue Total Change, value_format: !!null '',
        value_format_name: percent_2, calculation_type: percent_difference_from_previous,
        table_calculation: revenue_total_change, args: [platform_ltv_forecast.revenue_total],
        _kind_hint: measure, _type_hint: number}]
    custom_color_enabled: true
    show_single_value_title: false
    show_comparison: true
    comparison_type: change
    comparison_reverse_colors: false
    show_comparison_label: false
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    series_types: {}
    defaults_version: 1
    listen: {}
    row: 2
    col: 12
    width: 2
    height: 2
  - title: Revenue IOS
    name: Revenue IOS
    model: platform_ltv_forecast
    explore: platform_ltv_forecast
    type: single_value
    fields: [platform_ltv_forecast.revenue_ios]
    limit: 500
    custom_color_enabled: true
    show_single_value_title: false
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    series_types: {}
    defaults_version: 1
    listen: {}
    row: 4
    col: 12
    width: 2
    height: 2
  - title: Revenue Android
    name: Revenue Android
    model: platform_ltv_forecast
    explore: platform_ltv_forecast
    type: single_value
    fields: [platform_ltv_forecast.revenue_android]
    limit: 500
    custom_color_enabled: true
    show_single_value_title: false
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    series_types: {}
    defaults_version: 1
    listen: {}
    row: 6
    col: 12
    width: 2
    height: 2
  - title: DAU Total
    name: DAU Total
    model: platform_ltv_forecast
    explore: platform_ltv_forecast
    type: single_value
    fields: [platform_ltv_forecast.dau_total, platform_ltv_forecast.install_date]
    sorts: [platform_ltv_forecast.install_date desc]
    limit: 500
    dynamic_fields: [{category: table_calculation, label: DAU Total Change, value_format: !!null '',
        value_format_name: percent_2, calculation_type: percent_difference_from_previous,
        table_calculation: dau_total_change, args: [platform_ltv_forecast.dau_total],
        _kind_hint: measure, _type_hint: number}]
    custom_color_enabled: true
    show_single_value_title: false
    show_comparison: true
    comparison_type: change
    comparison_reverse_colors: false
    show_comparison_label: false
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    conditional_formatting: [{type: equal to, value: !!null '', background_color: "#1A73E8",
        font_color: !!null '', color_application: {collection_id: 7c56cc21-66e4-41c9-81ce-a60e1c3967b2,
          palette_id: 56d0c358-10a0-4fd6-aa0b-b117bef527ab}, bold: false, italic: false,
        strikethrough: false, fields: !!null ''}]
    series_types: {}
    defaults_version: 1
    listen: {}
    row: 2
    col: 8
    width: 2
    height: 2
  - title: DAU IOS
    name: DAU IOS
    model: platform_ltv_forecast
    explore: platform_ltv_forecast
    type: single_value
    fields: [platform_ltv_forecast.dau_ios]
    limit: 500
    custom_color_enabled: true
    show_single_value_title: false
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    series_types: {}
    defaults_version: 1
    listen: {}
    row: 4
    col: 8
    width: 2
    height: 2
  - title: DAU Android
    name: DAU Android
    model: platform_ltv_forecast
    explore: platform_ltv_forecast
    type: single_value
    fields: [platform_ltv_forecast.dau_android]
    limit: 500
    custom_color_enabled: true
    show_single_value_title: false
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    series_types: {}
    defaults_version: 1
    listen: {}
    row: 6
    col: 8
    width: 2
    height: 2
  - title: ARPDAU Total
    name: ARPDAU Total
    model: platform_ltv_forecast
    explore: platform_ltv_forecast
    type: single_value
    fields: [platform_ltv_forecast.arpdau_total, platform_ltv_forecast.install_time]
    sorts: [platform_ltv_forecast.install_time desc]
    limit: 500
    dynamic_fields: [{category: table_calculation, label: ARPDAU Total Change, value_format: !!null '',
        value_format_name: percent_2, calculation_type: percent_difference_from_previous,
        table_calculation: arpdau_total_change, args: [platform_ltv_forecast.arpdau_total],
        _kind_hint: measure, _type_hint: number}]
    custom_color_enabled: true
    show_single_value_title: false
    show_comparison: true
    comparison_type: change
    comparison_reverse_colors: false
    show_comparison_label: false
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    series_types: {}
    defaults_version: 1
    listen: {}
    row: 2
    col: 10
    width: 2
    height: 2
  - title: ARPDAU IOS
    name: ARPDAU IOS
    model: platform_ltv_forecast
    explore: platform_ltv_forecast
    type: single_value
    fields: [platform_ltv_forecast.arpdau_ios]
    limit: 500
    custom_color_enabled: true
    show_single_value_title: false
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    series_types: {}
    defaults_version: 1
    listen: {}
    row: 4
    col: 10
    width: 2
    height: 2
  - title: ARPDAU Android
    name: ARPDAU Android
    model: platform_ltv_forecast
    explore: platform_ltv_forecast
    type: single_value
    fields: [platform_ltv_forecast.arpdau_android]
    limit: 500
    custom_color_enabled: true
    show_single_value_title: false
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    series_types: {}
    defaults_version: 1
    listen: {}
    row: 6
    col: 10
    width: 2
    height: 2
  - title: D0 ROAS Total
    name: D0 ROAS Total
    model: platform_ltv_forecast
    explore: platform_ltv_forecast
    type: single_value
    fields: [platform_ltv_forecast.d0_roas, platform_ltv_forecast.install_time]
    sorts: [platform_ltv_forecast.install_time desc]
    limit: 500
    dynamic_fields: [{category: table_calculation, label: D0 ROAS Total Change, value_format: !!null '',
        value_format_name: percent_2, calculation_type: percent_difference_from_previous,
        table_calculation: d0_roas_total_change, args: [platform_ltv_forecast.d0_roas],
        _kind_hint: measure, _type_hint: number}]
    custom_color_enabled: true
    show_single_value_title: false
    show_comparison: true
    comparison_type: change
    comparison_reverse_colors: false
    show_comparison_label: false
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    series_types: {}
    defaults_version: 1
    listen: {}
    row: 2
    col: 14
    width: 2
    height: 2
  - title: D0 ROAS IOS
    name: D0 ROAS IOS
    model: platform_ltv_forecast
    explore: platform_ltv_forecast
    type: single_value
    fields: [platform_ltv_forecast.d0_roas_ios]
    limit: 500
    custom_color_enabled: true
    show_single_value_title: false
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    series_types: {}
    defaults_version: 1
    listen: {}
    row: 4
    col: 14
    width: 2
    height: 2
  - title: D0 ROAS Android
    name: D0 ROAS Android
    model: platform_ltv_forecast
    explore: platform_ltv_forecast
    type: single_value
    fields: [platform_ltv_forecast.d0_roas_android]
    limit: 500
    custom_color_enabled: true
    show_single_value_title: false
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    series_types: {}
    defaults_version: 1
    listen: {}
    row: 6
    col: 14
    width: 2
    height: 2
  - title: D30 Exp ROAS Total
    name: D30 Exp ROAS Total
    model: platform_ltv_forecast
    explore: platform_ltv_forecast
    type: single_value
    fields: [platform_ltv_forecast.d30_exp_roas, platform_ltv_forecast.install_date]
    sorts: [platform_ltv_forecast.install_date desc]
    limit: 500
    dynamic_fields: [{category: table_calculation, label: D30 Exp ROAS Total, value_format: !!null '',
        value_format_name: percent_2, calculation_type: percent_difference_from_previous,
        table_calculation: d30_exp_roas_total, args: [platform_ltv_forecast.d30_exp_roas],
        _kind_hint: measure, _type_hint: number}]
    custom_color_enabled: true
    show_single_value_title: false
    show_comparison: true
    comparison_type: change
    comparison_reverse_colors: false
    show_comparison_label: false
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    single_value_title: D30 Exp ROAS
    series_types: {}
    defaults_version: 1
    listen: {}
    row: 2
    col: 16
    width: 2
    height: 2
  - title: D30 Exp ROAS IOS
    name: D30 Exp ROAS IOS
    model: platform_ltv_forecast
    explore: platform_ltv_forecast
    type: single_value
    fields: [platform_ltv_forecast.d30_exp_roas_ios]
    limit: 500
    custom_color_enabled: true
    show_single_value_title: false
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    series_types: {}
    defaults_version: 1
    listen: {}
    row: 4
    col: 16
    width: 2
    height: 2
  - title: D30 Exp ROAS Android
    name: D30 Exp ROAS Android
    model: platform_ltv_forecast
    explore: platform_ltv_forecast
    type: single_value
    fields: [platform_ltv_forecast.d30_exp_roas_android]
    limit: 500
    custom_color_enabled: true
    show_single_value_title: false
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    series_types: {}
    defaults_version: 1
    listen: {}
    row: 6
    col: 16
    width: 2
    height: 2
  - title: D60 Exp ROAS Total
    name: D60 Exp ROAS Total
    model: platform_ltv_forecast
    explore: platform_ltv_forecast
    type: single_value
    fields: [platform_ltv_forecast.d60_exp_roas, platform_ltv_forecast.install_time]
    sorts: [platform_ltv_forecast.install_time desc]
    limit: 500
    dynamic_fields: [{category: table_calculation, label: D60 Exp ROAS Total Change,
        value_format: !!null '', value_format_name: percent_2, calculation_type: percent_difference_from_previous,
        table_calculation: d60_exp_roas_total_change, args: [platform_ltv_forecast.d60_exp_roas],
        _kind_hint: measure, _type_hint: number}]
    custom_color_enabled: true
    show_single_value_title: false
    show_comparison: true
    comparison_type: change
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    single_value_title: D60 Exp ROAS
    series_types: {}
    defaults_version: 1
    listen: {}
    row: 16
    col: 0
    width: 2
    height: 3
  - title: D60 Exp ROAS IOS
    name: D60 Exp ROAS IOS
    model: platform_ltv_forecast
    explore: platform_ltv_forecast
    type: single_value
    fields: [platform_ltv_forecast.d60_exp_roas_ios]
    limit: 500
    custom_color_enabled: true
    show_single_value_title: false
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    series_types: {}
    defaults_version: 1
    listen: {}
    row: 16
    col: 2
    width: 2
    height: 2
  - title: D60 Exp ROAS Android
    name: D60 Exp ROAS Android
    model: platform_ltv_forecast
    explore: platform_ltv_forecast
    type: single_value
    fields: [platform_ltv_forecast.d60_exp_roas_android]
    limit: 500
    custom_color_enabled: true
    show_single_value_title: false
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    series_types: {}
    defaults_version: 1
    listen: {}
    row: 16
    col: 4
    width: 2
    height: 2
  - title: D90 Exp ROAS Total
    name: D90 Exp ROAS Total
    model: platform_ltv_forecast
    explore: platform_ltv_forecast
    type: single_value
    fields: [platform_ltv_forecast.d90_exp_roas, platform_ltv_forecast.install_time]
    sorts: [platform_ltv_forecast.install_time desc]
    limit: 500
    dynamic_fields: [{category: table_calculation, label: D90 Exp ROAS Total Change,
        value_format: !!null '', value_format_name: percent_2, calculation_type: percent_difference_from_previous,
        table_calculation: d90_exp_roas_total_change, args: [platform_ltv_forecast.d90_exp_roas],
        _kind_hint: measure, _type_hint: number}]
    custom_color_enabled: true
    show_single_value_title: false
    show_comparison: true
    comparison_type: change
    comparison_reverse_colors: false
    show_comparison_label: false
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    single_value_title: D90 Exp ROAS
    series_types: {}
    defaults_version: 1
    listen: {}
    row: 2
    col: 18
    width: 2
    height: 2
  - title: D90 Exp ROAS IOS
    name: D90 Exp ROAS IOS
    model: platform_ltv_forecast
    explore: platform_ltv_forecast
    type: single_value
    fields: [platform_ltv_forecast.d90_exp_roas_ios]
    limit: 500
    custom_color_enabled: true
    show_single_value_title: false
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    series_types: {}
    defaults_version: 1
    listen: {}
    row: 4
    col: 18
    width: 2
    height: 2
  - title: D90 Exp ROAS Android
    name: D90 Exp ROAS Android
    model: platform_ltv_forecast
    explore: platform_ltv_forecast
    type: single_value
    fields: [platform_ltv_forecast.d90_exp_roas_android]
    limit: 500
    custom_color_enabled: true
    show_single_value_title: false
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    series_types: {}
    defaults_version: 1
    listen: {}
    row: 6
    col: 18
    width: 2
    height: 2
  - title: D120 Exp ROAS Total
    name: D120 Exp ROAS Total
    model: platform_ltv_forecast
    explore: platform_ltv_forecast
    type: single_value
    fields: [platform_ltv_forecast.d120_exp_roas, platform_ltv_forecast.install_time]
    sorts: [platform_ltv_forecast.install_time desc]
    limit: 500
    dynamic_fields: [{category: table_calculation, label: D120 Exp ROAS Total Change,
        value_format: !!null '', value_format_name: percent_2, calculation_type: percent_difference_from_previous,
        table_calculation: d120_exp_roas_total_change, args: [platform_ltv_forecast.d120_exp_roas],
        _kind_hint: measure, _type_hint: number}]
    custom_color_enabled: true
    show_single_value_title: false
    show_comparison: true
    comparison_type: change
    comparison_reverse_colors: false
    show_comparison_label: false
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    single_value_title: D120 Exp ROAS
    series_types: {}
    defaults_version: 1
    listen: {}
    row: 2
    col: 20
    width: 2
    height: 2
  - title: D120 Exp ROAS IOS
    name: D120 Exp ROAS IOS
    model: platform_ltv_forecast
    explore: platform_ltv_forecast
    type: single_value
    fields: [platform_ltv_forecast.d120_exp_roas_ios]
    limit: 500
    custom_color_enabled: true
    show_single_value_title: false
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    series_types: {}
    defaults_version: 1
    listen: {}
    row: 4
    col: 20
    width: 2
    height: 2
  - title: D120 Exp ROAS Android
    name: D120 Exp ROAS Android
    model: platform_ltv_forecast
    explore: platform_ltv_forecast
    type: single_value
    fields: [platform_ltv_forecast.d120_exp_roas_android]
    limit: 500
    custom_color_enabled: true
    show_single_value_title: false
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    series_types: {}
    defaults_version: 1
    listen: {}
    row: 6
    col: 20
    width: 2
    height: 2
  - title: D180 Exp ROAS Total
    name: D180 Exp ROAS Total
    model: platform_ltv_forecast
    explore: platform_ltv_forecast
    type: single_value
    fields: [platform_ltv_forecast.d180_exp_roas, platform_ltv_forecast.install_time]
    sorts: [platform_ltv_forecast.install_time desc]
    limit: 500
    dynamic_fields: [{category: table_calculation, label: D180 Exp ROAS Total Change,
        value_format: !!null '', value_format_name: percent_2, calculation_type: percent_difference_from_previous,
        table_calculation: d180_exp_roas_total_change, args: [platform_ltv_forecast.d180_exp_roas],
        _kind_hint: measure, _type_hint: number}]
    custom_color_enabled: true
    show_single_value_title: false
    show_comparison: true
    comparison_type: change
    comparison_reverse_colors: false
    show_comparison_label: false
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    single_value_title: D180 Exp ROAS
    series_types: {}
    defaults_version: 1
    listen: {}
    row: 2
    col: 22
    width: 2
    height: 2
  - title: D180 Exp ROAS IOS
    name: D180 Exp ROAS IOS
    model: platform_ltv_forecast
    explore: platform_ltv_forecast
    type: single_value
    fields: [platform_ltv_forecast.d180_exp_roas_ios]
    limit: 500
    custom_color_enabled: true
    show_single_value_title: false
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    series_types: {}
    defaults_version: 1
    listen: {}
    row: 4
    col: 22
    width: 2
    height: 2
  - title: D180 Exp ROAS Android
    name: D180 Exp ROAS Android
    model: platform_ltv_forecast
    explore: platform_ltv_forecast
    type: single_value
    fields: [platform_ltv_forecast.d180_exp_roas_android]
    limit: 500
    custom_color_enabled: true
    show_single_value_title: false
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    series_types: {}
    defaults_version: 1
    listen: {}
    row: 6
    col: 22
    width: 2
    height: 2
  - title: 'KPIs with Tier Breakdown '
    name: 'KPIs with Tier Breakdown '
    model: platform_ltv_forecast
    explore: platform_tier_ltv_forecast
    type: looker_grid
    fields: [platform_tier_ltv_forecast.tier, platform_tier_ltv_forecast.platform,
      platform_tier_ltv_forecast.installs, platform_tier_ltv_forecast.cost, platform_tier_ltv_forecast.cpi_total,
      platform_tier_ltv_forecast.revenue_total, platform_tier_ltv_forecast.arpdau_total,
      platform_tier_ltv_forecast.d0_roas, platform_tier_ltv_forecast.d30_exp_roas,
      platform_tier_ltv_forecast.d60_exp_roas, platform_tier_ltv_forecast.d90_exp_roas,
      platform_tier_ltv_forecast.d120_exp_roas, platform_tier_ltv_forecast.d180_exp_roas]
    sorts: [platform_tier_ltv_forecast.platform desc]
    limit: 500
    show_view_names: false
    show_row_numbers: false
    transpose: false
    truncate_text: true
    hide_totals: false
    hide_row_totals: false
    size_to_fit: true
    table_theme: white
    limit_displayed_rows: false
    enable_conditional_formatting: false
    header_text_alignment: left
    header_font_size: '12'
    rows_font_size: '12'
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    show_sql_query_menu_options: false
    show_totals: true
    show_row_totals: true
    series_labels:
      platform_tier_ltv_forecast.cpi_total: CPI
      platform_tier_ltv_forecast.revenue_total: Revenue
      platform_tier_ltv_forecast.arpdau_total: ARPDAU
      platform_tier_ltv_forecast.d0_roas: D0 ROAS
      platform_tier_ltv_forecast.d30_exp_roas: D30 Exp ROAS
      platform_tier_ltv_forecast.d60_exp_roas: D60 Exp ROAS
      platform_tier_ltv_forecast.d90_exp_roas: D90 Exp ROAS
      platform_tier_ltv_forecast.d120_exp_roas: D120 Exp ROAS
      platform_tier_ltv_forecast.d180_exp_roas: D180 Exp ROAS
    series_cell_visualizations:
      platform_tier_ltv_forecast.cpi_total:
        is_active: true
        palette:
          palette_id: 7e902bec-1694-2390-aee4-9d8dd73e74de
          collection_id: 7c56cc21-66e4-41c9-81ce-a60e1c3967b2
          custom_colors:
          - "#7CB342"
          - "#ffffff"
          - "#e8281d"
      platform_tier_ltv_forecast.d0_roas:
        is_active: true
        palette:
          palette_id: 409dca85-8004-e5e1-2887-e5d0e038ea91
          collection_id: 7c56cc21-66e4-41c9-81ce-a60e1c3967b2
          custom_colors:
          - "#e8281d"
          - "#ffffff"
          - "#7CB342"
      platform_tier_ltv_forecast.d30_exp_roas:
        is_active: false
        palette:
          palette_id: d3acfda2-a9bc-9de4-05ad-ceff7e56bba4
          collection_id: 7c56cc21-66e4-41c9-81ce-a60e1c3967b2
          custom_colors:
          - "#e8281d"
          - "#ffffff"
          - "#7CB342"
      platform_tier_ltv_forecast.d60_exp_roas:
        is_active: false
        palette:
          palette_id: 7b1a2864-cc5f-8838-3e14-3712b9707496
          collection_id: 7c56cc21-66e4-41c9-81ce-a60e1c3967b2
          custom_colors:
          - "#e8281d"
          - "#ffffff"
          - "#7CB342"
      platform_tier_ltv_forecast.d90_exp_roas:
        is_active: false
        palette:
          palette_id: 57725a0c-b4b9-296c-badb-44b222a0a259
          collection_id: 7c56cc21-66e4-41c9-81ce-a60e1c3967b2
          custom_colors:
          - "#e8281d"
          - "#ffffff"
          - "#7CB342"
      platform_tier_ltv_forecast.d120_exp_roas:
        is_active: false
        palette:
          palette_id: a953087f-3153-d674-902d-b61037c5b32d
          collection_id: 7c56cc21-66e4-41c9-81ce-a60e1c3967b2
          custom_colors:
          - "#e8281d"
          - "#ffffff"
          - "#7CB342"
      platform_tier_ltv_forecast.arpdau_total:
        is_active: true
        palette:
          palette_id: 5a19d71a-e16c-9eb3-6af5-23a3d9e0bca4
          collection_id: 7c56cc21-66e4-41c9-81ce-a60e1c3967b2
          custom_colors:
          - "#e8281d"
          - "#ffffff"
          - "#7CB342"
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: ''
    legend_position: center
    point_style: none
    show_value_labels: false
    label_density: 25
    x_axis_scale: auto
    y_axis_combined: true
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    defaults_version: 1
    hidden_series: []
    series_types: {}
    listen:
      Install Date: platform_tier_ltv_forecast.install_date
    row: 8
    col: 0
    width: 24
    height: 8
  - name: Android
    type: text
    title_text: Android
    body_text: ''
    row: 6
    col: 0
    width: 2
    height: 2
  - name: ''
    type: text
    title_text: ''
    body_text: ''
    row: 0
    col: 0
    width: 2
    height: 2
  - name: Installs
    type: text
    title_text: Installs
    body_text: ''
    row: 0
    col: 2
    width: 2
    height: 2
  - name: Total
    type: text
    title_text: Total
    body_text: ''
    row: 2
    col: 0
    width: 2
    height: 2
  - name: Cost
    type: text
    title_text: Cost
    body_text: ''
    row: 0
    col: 4
    width: 2
    height: 2
  - name: IOS
    type: text
    title_text: IOS
    body_text: ''
    row: 4
    col: 0
    width: 2
    height: 2
  - name: CPI
    type: text
    title_text: CPI
    body_text: ''
    row: 0
    col: 6
    width: 2
    height: 2
  - name: DAU
    type: text
    title_text: DAU
    body_text: ''
    row: 0
    col: 8
    width: 2
    height: 2
  - name: Revenue
    type: text
    title_text: Revenue
    body_text: ''
    row: 0
    col: 12
    width: 2
    height: 2
  - name: D180 ROAS
    type: text
    title_text: ''
    body_text: D180 ROAS Exp
    row: 0
    col: 22
    width: 2
    height: 2
  - name: D0 ROAS
    type: text
    title_text: ''
    body_text: D0 ROAS
    row: 0
    col: 14
    width: 2
    height: 2
  - name: ARPDAU
    type: text
    title_text: ARPDAU
    body_text: ''
    row: 0
    col: 10
    width: 2
    height: 2
  - name: D30 ROAS Exp
    type: text
    title_text: D30 ROAS Exp
    body_text: ''
    row: 0
    col: 16
    width: 2
    height: 2
  - name: D90 ROAS Exp
    type: text
    title_text: D90 \n ROAS Exp
    body_text: ''
    row: 0
    col: 18
    width: 2
    height: 2
  - name: D120 ROAS Exp
    type: text
    title_text: D120 ROAS Exp
    body_text: ''
    row: 0
    col: 20
    width: 2
    height: 2
  filters:
  - name: Install Date
    title: Install Date
    type: field_filter
    default_value: 2 days ago
    allow_multiple_values: false
    required: True
    ui_config:
      type: advanced
      display: popover
      options: []
    model: platform_ltv_forecast
    explore: platform_ltv_forecast
    listens_to_filters: []
    field: platform_ltv_forecast.install_date
